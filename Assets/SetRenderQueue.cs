﻿
using UnityEngine;

public class SetRenderQueue : MonoBehaviour
{
    private Material _meshMaterial;
    public int _targetQueue;
    
    void Start()
    {
        _meshMaterial = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        _meshMaterial.renderQueue = _targetQueue;
    }
}
