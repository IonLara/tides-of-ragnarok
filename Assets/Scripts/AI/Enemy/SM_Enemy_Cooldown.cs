﻿using UnityEngine;
using UnityEngine.AI;

public class SM_Enemy_Cooldown : StateMachineBehaviour
{
    [SerializeField]
    private float _attackCooldown = 2;
    private float _timer = 0f;
    
    private bool _isDone = false;

    private NavMeshAgent _enemyAgent;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       animator.ResetTrigger("Cooldown");

        _enemyAgent = animator.GetComponentInParent<NavMeshAgent>();

        _isDone = false;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       _timer += Time.deltaTime;

       if(_timer >= _attackCooldown && _isDone == false)
       {
           animator.SetTrigger("Idle");
           _isDone = true;
       }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       _timer = 0f;
    }
}
