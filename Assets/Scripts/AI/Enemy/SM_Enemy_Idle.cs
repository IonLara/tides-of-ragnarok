﻿using UnityEngine;
using UnityEngine.AI;

public class SM_Enemy_Idle : StateMachineBehaviour
{
    private NavMeshAgent _enemyAgent;

    private Animator _animator;

    private Transform _playerTransform;

    private EnemyAttack _enemyScript;

    [SerializeField]
    private float _attackRange = 6f;
    [SerializeField]
    private float _detectRange = 26f;
    [SerializeField]
    private float _chaseRange = 30f;

    private bool _isDone = false;

    [SerializeField]
    private Vector3 _enemyDestination = new Vector3(0f, 0f, 0f);

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Idle");

        _animator = animator;
        _enemyAgent = animator.GetComponentInParent<NavMeshAgent>();
        _playerTransform = FindObjectOfType<RowingMovement>().transform;

        _isDone = false;

        _enemyScript = animator.GetComponentInParent<EnemyAttack>();
        _enemyScript.StopMovingSound();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(_playerTransform.position, animator.transform.position) <= _attackRange && _isDone == false)
        {
            animator.SetTrigger("Attack");
            _isDone = true;
        }

        if (Vector3.Distance(_playerTransform.position, animator.transform.position) <= _detectRange && _isDone == false)
        {
            animator.SetTrigger("Moving");
            _isDone = true;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
