﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Animator), typeof(Rigidbody))]
public class EnemyAttack : MonoBehaviour
{
    [SerializeField]
    private int _enemyAttack = 2;

    [SerializeField]
    private float _minAttackSpeed = 5;
    
    [SerializeField]
    private float _attackForce = 100f;

    [SerializeField]
    private GameObject _deathParticles;

    [SerializeField]
    private Transform _playerTransform;

    private Rigidbody _rigidBody;

    [SerializeField]
    private GameObject _mistParticles;
    [SerializeField]
    private  Transform _snakeHead;
    
    [Header("Sound")]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _movingSound;
    [SerializeField]
    private AudioClip[] _hissingSounds;
    [SerializeField]
    private AudioClip[] _attackSounds;

    private bool _isMoving = false;

    public static List<EnemyAttack> Enemies = new List<EnemyAttack>();

    private void Awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody>();
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        Enemies.Add(this);
    }

    private void OnDisable()
    {
        Enemies.Remove(this);
        Instantiate(_deathParticles, transform.position, Quaternion.Euler(90,0,0));
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<BoatHealth>() != null)
        {
            if (other.relativeVelocity.magnitude >= _minAttackSpeed)
            {
                other.gameObject.GetComponent<BoatHealth>().ReceiveDamage(_enemyAttack, other.contacts[0].point, other.contacts[0].normal, _rigidBody.velocity.magnitude);
                // Debug.Log("Damage done, velocity was: " + other.relativeVelocity.magnitude);
            }
            else
            {
                // Debug.Log("Not enough speed! Speed was: " + other.relativeVelocity.magnitude);
            }            
        }
    }

    private void Attack()
    {
        _rigidBody.AddForce((_playerTransform.position - transform.position).normalized * _attackForce, ForceMode.Force);
    }

    private void SpawnMist()
    {
        Instantiate(_mistParticles, _snakeHead);
    }

    public void PlayMovingSound()
    {
        if (_isMoving == false)
        {
            _audioSource.loop = true;
            _audioSource.clip = _movingSound;
            _audioSource.Play();
            _isMoving = true;            
        }

        int _clipIndex = UnityEngine.Random.Range(0, _hissingSounds.Length);
        _audioSource.PlayOneShot(_hissingSounds[_clipIndex]);
    }

    public void StopMovingSound()
    {
        _audioSource.Stop();
        _isMoving = false;
    }

    public void PlayAttackSound()
    {
        int _clipIndex = UnityEngine.Random.Range(0, _attackSounds.Length);
        _audioSource.PlayOneShot(_attackSounds[_clipIndex]);
    }

    public void SetTargetTransform(Transform NewTransform)
    {
        _playerTransform = NewTransform;
    }
}
