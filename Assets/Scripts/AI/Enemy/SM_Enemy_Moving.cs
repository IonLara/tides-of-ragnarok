﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Animator), typeof(Rigidbody), typeof(NavMeshAgent))]
public class SM_Enemy_Moving : StateMachineBehaviour
{
    // private Collider[] _rangeColliders = new Collider[4];
    private NavMeshAgent _enemyAgent;

    private Animator _animator;

    private Transform _playerTransform;

    private bool _isDone = false;

    private EnemyAttack _enemyScript;

    [SerializeField]
    private float _attackRange = 6f;
    [SerializeField]
    private float _detectRange = 26f;
    [SerializeField]
    private float _chaseRange = 30f;

    [SerializeField]
    private Vector3 _enemyDestination = new Vector3(0f, 0f, 0f);

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Moving");
        _animator = animator;

        _playerTransform = FindObjectOfType<RowingMovement>().transform;

        _enemyAgent = animator.GetComponentInParent<NavMeshAgent>();

        _isDone = false;

        _enemyAgent.isStopped = false;

        _enemyScript = animator.GetComponentInParent<EnemyAttack>();
        _enemyScript.PlayMovingSound();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _enemyAgent.SetDestination(_playerTransform.position);

        if (Vector3.Distance(_playerTransform.position, animator.transform.position) <= _attackRange && _isDone == false)
        {
            animator.SetTrigger("Attack");
            _isDone = true;
            _enemyAgent.isStopped = true;
        }

        if (Vector3.Distance(_playerTransform.position, animator.transform.position) >= _chaseRange && _isDone == false)
        {
            animator.SetTrigger("Idle");
            _isDone = true;
            _enemyAgent.isStopped = true;
        }


    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _enemyScript.StopMovingSound();
    }
}
