﻿using UnityEngine;
using UnityEngine.AI;

public class SM_Enemy_Frozen : StateMachineBehaviour
{
    private NavMeshAgent _enemyAgent;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
      animator.ResetTrigger("Frozen");
      _enemyAgent = animator.GetComponentInParent<NavMeshAgent>();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
