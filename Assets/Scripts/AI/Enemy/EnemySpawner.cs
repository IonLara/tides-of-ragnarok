﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private BoatHealth _playerHealth; 
    
    [SerializeField]
    private List<Transform> _spawnPoints = new List<Transform>();

    [SerializeField]
    private List<GameObject> _spawnedEnemies = new List<GameObject>();
    private int _enemiesIndex = 0;

    [SerializeField]
    private GameObject _enemyPrefab;
    
    [SerializeField]
    private LayerMask _enemyCheckMask = 0;

    [SerializeField]
    private float _waveTime = 5f;

    private void OnEnable()
    {
        _playerHealth.PlayerDied += StopSpawning;
        _playerHealth.PlayerRespawned += ResetCount;
        StartCoroutine(SpawnEnemies());
    }

    private void OnDisable()
    {
        _playerHealth.PlayerDied -= StopSpawning;
        _playerHealth.PlayerRespawned -= ResetCount;
    }
    
    private void Awake()
    {
        GetComponentsInChildren<Transform>(_spawnPoints);
    }

    private IEnumerator SpawnEnemies()
    {
        while(enabled || gameObject.activeSelf)
        {
            for (int i = 0; i < _spawnPoints.Count; i++)
            {
                if(_spawnPoints[i] == transform)
                {
                    continue;
                }

                RaycastHit hit;
                if(Physics.SphereCast (_spawnPoints[i].position + Vector3.up * 10f, 3f, Vector3.down, out hit, 10f, _enemyCheckMask.value) == false)
                //if(Physics.OverlapSphere (_spawnPoints[i].position, 1f, _enemyCheckMask.value).Length == 0)
                {
                    GameObject _spawnedEnemy = Instantiate(_enemyPrefab, _spawnPoints[i]);

                    _spawnedEnemies.Add(_spawnedEnemy);
                    _spawnedEnemies[_enemiesIndex].GetComponent<EnemyAttack>().SetTargetTransform(transform);

                    _enemiesIndex++;
                }
            }
            yield return new WaitForSeconds(_waveTime);
        }
    //    StartCoroutine(SpawnEnemies());
    }

    public void StopSpawning()
    {
        StopAllCoroutines();
    }
    
    public void ResetCount()
    {
        for (int i = 0; i < _spawnedEnemies.Count; i++)
        {
            Destroy(_spawnedEnemies[i]);
            Debug.Log("Destroyed Enemy " + i + ", " + (_spawnedEnemies.Count-i) + " remaining");
        }

        _enemiesIndex = 0;
        _spawnedEnemies.Clear();

        this.gameObject.SetActive(false);
    }
}
