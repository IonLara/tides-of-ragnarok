﻿using UnityEngine;

public class TEST_EnemyCoroutine : MonoBehaviour
{
    [SerializeField]
    private float _attackRange = 6f;
    [SerializeField]
    private float _detectRange = 6f;
    [SerializeField]
    private float _chaseRange = 6f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _attackRange);
        Gizmos.DrawWireSphere(transform.position, _detectRange);
        Gizmos.DrawWireSphere(transform.position, _chaseRange);
    }
}
