﻿using UnityEngine;
using UnityEngine.AI;

public class SM_Enemy_Attacking : StateMachineBehaviour
{
    private NavMeshAgent _enemyAgent;

    private Animator _animator;

    private Transform _playerTransform;

    private Rigidbody _rigidBody;

    private EnemyAttack _enemyScript;

    [SerializeField]
    private float _attackRange = 6f;
    [SerializeField]
    private float _detectRange = 26f;
    [SerializeField]
    private float _chaseRange = 30f;

    [SerializeField]
    private Vector3 _enemyDestination = new Vector3(0f, 0f, 0f);

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("Attack");

        _rigidBody = animator.GetComponentInParent<Rigidbody>();
        _animator = animator;
        _enemyAgent = animator.GetComponentInParent<NavMeshAgent>();
        _playerTransform = FindObjectOfType<RowingMovement>().transform;

        _enemyScript = animator.GetComponentInParent<EnemyAttack>();
        _enemyScript.PlayAttackSound();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Distance(_playerTransform.position, animator.transform.position) >= _attackRange)
        {
            animator.SetTrigger("Moving");
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
