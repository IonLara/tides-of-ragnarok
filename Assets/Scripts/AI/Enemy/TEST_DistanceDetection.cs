﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TEST_DistanceDetection : MonoBehaviour
{
    public float _attackRange = 10f;

    private Transform _player;
    
    void Start()
    {
        _player = FindObjectOfType<RowingMovement>().transform;
    }

    void Update()
    {
        if (Vector3.Distance(_player.position, transform.position) <= _attackRange)
        {
            Debug.Log("Attacked!");
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _attackRange);
    }
}
