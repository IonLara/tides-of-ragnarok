﻿using UnityEngine;

public class RunePickup : MonoBehaviour
{
    //If you have issues with type checking you can use an enum
    public RuneType runeType;

    [SerializeField]
    public ARune _runeType;
}

public enum RuneType
{
    Lightning,
    Ice,
    Health
}