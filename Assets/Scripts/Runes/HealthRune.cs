﻿using System;
using UnityEngine;

public class HealthRune : ARune
{
    [SerializeField]
    private int _restoredHealth = 3;

    [SerializeField]
    private GameObject _healthParticles;

    public override bool ActivateRune(Transform playerTransform)
    {
        bool _effective;
        _effective = playerTransform.gameObject.GetComponent<BoatHealth>().Heal(_restoredHealth);
        Instantiate(_healthParticles, (playerTransform.position - new Vector3(0,0,0.5f)), playerTransform.rotation, playerTransform);
        
        return true;
    }
}
