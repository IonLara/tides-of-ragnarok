﻿using UnityEngine;

public abstract class ARune : MonoBehaviour
{
    public abstract bool ActivateRune(Transform playerTransform);
}
