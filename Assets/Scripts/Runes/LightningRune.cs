﻿using System.Collections;
using UnityEngine;

public class LightningRune : ARune
{
    [SerializeField]
    private float _range = 10f;

    [SerializeField]
    private float _animationLength = 3;

    private Vector3 _verticalOffset = new Vector3(0,17.5f,0);
    private Vector3 _startingscale = new Vector3(3.7f, 0.0f, 3.7f);

    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private GameObject _lightningSprite;

    [SerializeField]
    private LayerMask _enemyLayer = 12;

    public override bool ActivateRune(Transform playerTransform)
    {
        Collider[] _affectedEnemies = Physics.OverlapSphere(playerTransform.position, _range, _enemyLayer);

        if (_affectedEnemies.Length == 0)
        {
            Debug.Log("No nearby enemies");
            return false;
        }
        else
        {
            for (int i = 0; i < _affectedEnemies.Length; i++)
            {
                _gameManager.EnemyKilled(_affectedEnemies[i].gameObject);
                // Destroy(_affectedEnemies[i].gameObject);
                Debug.Log("Lightning!!!!");
            }

            return true;
        }
    }
}
