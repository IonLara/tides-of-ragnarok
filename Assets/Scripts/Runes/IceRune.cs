﻿using UnityEngine;

public class IceRune : ARune
{
    [SerializeField]
    private float _range = 10f;

    [SerializeField]
    private LayerMask _enemyLayer = 12;
     
    [SerializeField]
    private GameObject _snowParticles;

    public override bool ActivateRune(Transform playerTransform)
    {
        Collider[] _affectedEnemies = Physics.OverlapSphere(playerTransform.position, _range, _enemyLayer);

        if (_affectedEnemies.Length == 0)
        {
            Debug.Log("No nearby enemies");
            return false;
        }
        else
        {
            for (int i = 0; i < _affectedEnemies.Length; i++)
            {
                _affectedEnemies[i].GetComponent<Animator>().SetTrigger("Frozen");
                Instantiate(_snowParticles, (playerTransform.position - new Vector3(0,2,0)), playerTransform.rotation);
                Debug.Log("Icyyyy!!");

                return true;
            }
        }
        return false;
    }
}
