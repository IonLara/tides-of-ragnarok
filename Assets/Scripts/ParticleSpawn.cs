﻿using UnityEngine;

public class ParticleSpawn : MonoBehaviour
{
    [SerializeField]
    GameObject _particle01;
    [SerializeField]
    GameObject _particle02;
    [SerializeField]
    GameObject _particle03;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            Instantiate(_particle01,transform);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            Instantiate(_particle02,transform);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            Instantiate(_particle03,transform);
        }
    }
}
