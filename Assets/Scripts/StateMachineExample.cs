﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachineExample : MonoBehaviour
{
    public enum EnemyStates{
        Idle,
        Attacking,
        Cooldown
    }

    EnemyStates currentState;

    void ChangeState(EnemyStates newState){
        switch(newState){
            case EnemyStates.Idle:
            StartCoroutine(DoIdleState());
                break;
            case EnemyStates.Attacking:
            StartCoroutine(DoAttackingState());
                break;
            case EnemyStates.Cooldown:
            StartCoroutine(DoCooldownState());
                break;
            default:
                break;
        }
        currentState = newState;
    }

    bool spotPlayer = false;

    public IEnumerator DoIdleState(){
        //State Entry

        while(currentState == EnemyStates.Idle){
            //State middle

            if(spotPlayer){
                ChangeState(EnemyStates.Attacking);
            }

            yield return null;
        }

        //state exit
    }
    public IEnumerator DoAttackingState(){

        
        while(currentState == EnemyStates.Attacking){

            //State middle
            yield return null;
        }
    }

    public float attackCooldownTime = 10.0f;

    public IEnumerator DoCooldownState(){

        float coolDownTimer = 0.0f;
        while(currentState == EnemyStates.Attacking){
            coolDownTimer += Time.deltaTime;
            //State middle
            if(coolDownTimer >= attackCooldownTime){
                coolDownTimer = attackCooldownTime;
                ChangeState(EnemyStates.Attacking);
            }
            yield return null;
        }
    }
}
