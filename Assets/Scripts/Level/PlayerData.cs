using UnityEngine;

public struct PlayerData
{
    public Vector3 Position;
    public Quaternion Rotation;
    public int Health;
    public float Gold;
    public bool IceRune;
    public bool LightningRune;
    public bool HealthRune;
}
