using UnityEngine;

public struct EnemyInfo
{
    public Vector3 Position;
    public Quaternion Rotation;
}
