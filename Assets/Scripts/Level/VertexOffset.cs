﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexOffset : MonoBehaviour
{
    [SerializeField]
    private int _calledVertex = 100;

    Mesh mesh;
    Vector3[] vertices;
    int[] ids;
    public float Power = 0.1f;
    void Start()
    {
        mesh = GetComponent<MeshFilter>().mesh;
        vertices = mesh.vertices;

        List<int> temp = new List<int>(vertices.Length);

        for (int i = 0; i < vertices.Length; i++)
        {
            temp.Add(i);
        }
        Shuffle(temp);
        ids = temp.ToArray();
        // StartCoroutine(TickWater());
    }

    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(List<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    //TODO sample offset from texture + pan texture

    // IEnumerator TickWater()
    // {
    //     while (true)
    //     {
    //         yield return new WaitForSeconds(1);
    //         UpdateWater(ids.Length);
    //     }
    // }

    private void Update()
    {
        UpdateWater(_calledVertex);
    }

    int current = 0;
    void UpdateWater(int max = 10)
    {
        for (var i = 0; i < max; i++)
        {
            current = current + i;
            if (current >= ids.Length)
            {
                current = 0;
                //vertices[i].y = Mathf.Sin(Time.time);
            }
            vertices[current].y = Random.Range(0, 1f) * Power;
            if (i > max)
            {
                mesh.vertices = vertices;
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();
                break;
            }

            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            //current = 0;
            //mesh.vertices = vertices;
            //mesh.RecalculateBounds();
            //vertices[i].y = Mathf.Sin(Time.time);
        }

        // assign the local vertices array into the vertices array of the Mesh.
    }

}
