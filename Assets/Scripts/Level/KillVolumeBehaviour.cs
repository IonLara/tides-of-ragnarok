﻿using UnityEngine;

public class KillVolumeBehaviour : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<BoatHealth>().CheckPoint();
    }
}
