﻿using System.Collections.Generic;
using UnityEngine;

public class WhirlpoolBehaviour : MonoBehaviour
{
    private Vector3 _boatPosition = new Vector3();

    private Vector3 _forceDirection = new Vector3();

    private List<Rigidbody> _affectedRBs = new List<Rigidbody>();

    [SerializeField]
    private float _whirlpoolForce = 100f;
    [SerializeField]
    private float _inwardForceMultiplicator = 0.5f;
    [SerializeField]
    private float _torqueMult = 0.7f;

    private void OnTriggerEnter(Collider other)
    {
        _affectedRBs.Add(other.GetComponent<Rigidbody>());
    }
    private void OnTriggerExit(Collider other)
    {
        _affectedRBs.Remove(other.GetComponent<Rigidbody>());
    }

    private void FixedUpdate()
    {
        if (_affectedRBs != null)
        {
            for (int i = 0; i < _affectedRBs.Count; i++)
            {
                if (_affectedRBs[i] != null)
                {
                    _affectedRBs[i].AddForce((transform.position - _affectedRBs[i].gameObject.transform.position).normalized * _whirlpoolForce * _inwardForceMultiplicator, ForceMode.Force);

                    GetForceDirection(transform.position, _affectedRBs[i].gameObject.transform.position);
                    _affectedRBs[i].AddForce(_forceDirection.normalized * _whirlpoolForce, ForceMode.Force);

                    _affectedRBs[i].AddTorque(transform.up * _whirlpoolForce * _torqueMult * -1, ForceMode.Force);                    
                }
            }            
        }
    }    

    private void GetForceDirection(Vector3 whirlPool, Vector3 boat)
    {
        _forceDirection.x = (whirlPool.z - boat.z);
        _forceDirection.y = 0;
        _forceDirection.z = (whirlPool.x - boat.x) * -1;
        return;
    }
}
