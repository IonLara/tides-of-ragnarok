﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Checkpoint : MonoBehaviour
{

    public static Transform CurrentCheckpoint { get; private set; }
    public static float CheckpointTime {get; private set;}

    [SerializeField]
    private bool _DisableCheckpointOnSet = false;



    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<BoatHealth>();
        if (player == null) return;

        CurrentCheckpoint = transform;
        CheckpointTime = LevelTimer.Timer;

        if(_DisableCheckpointOnSet) gameObject.SetActive(false);
    }

}




// -------------------
// using System.Collections.Generic;
// using UnityEngine;

// [RequireComponent(typeof(Collider))]
// public class Checkpoint : MonoBehaviour
// {

//     public static Transform CurrentCheckpoint { get; private set; }

//     [SerializeField]
//     private RowingMovement _playerGold;
//     [SerializeField]
//     private RunesController _playerRunes;
//     [SerializeField]
//     private BoatHealth _playerHealth;

//     [SerializeField]
//     private bool _DisableCheckpointOnSet = false;


//     private void Awake()
//     {
//         GetComponent<Collider>().isTrigger = true;
//     }

//     private void OnTriggerEnter(Collider other)
//     {
//         var rowingMovement = other.GetComponent<RowingMovement>();
//         if (rowingMovement == null) return;
//         SetCurrentCheckpoint();
//         SavePlayerData();
//         // SaveEnemiesData();
//     }

//     // private static void SaveEnemiesData()
//     // {
//     //     // Get Enemies list into
//     //     var currentEnemiesInfo = new EnemiesContainer();
//     //     foreach (var enemy in EnemyAttack.Enemies)
//     //     {
//     //         var e = new EnemyInfo()
//     //         {
//     //             Position = enemy.transform.position,
//     //             Rotation = enemy.transform.rotation
//     //         };
//     //         currentEnemiesInfo.Enemies.Add(e);
//     //     }

//     //     var lastEnemiesInfo = JsonUtility.ToJson(currentEnemiesInfo, true);
//     //     PlayerPrefs.SetString("SavePoint_EnemiesInfo", lastEnemiesInfo);
//     // }

//     private void SavePlayerData()
//     {
//         //Populate the current payer info into currrentPlayerData
//         var currentPlayerData = new PlayerData()
//         {
//             Position = transform.position,
//             Rotation = transform.rotation,
//             Health = _playerHealth.BoatHealth,
//             Gold = _playerGold._currentGold,
//             LightningRune = _playerRunes._hasLightRune,
//             IceRune = _playerRunes._hasIceRune,
//             HealthRune = _playerRunes._hasHealthRune
//             // TODO: Complete This...
//         };
//         var lastPlayerState = JsonUtility.ToJson(currentPlayerData, true);
//         PlayerPrefs.SetString("SavePoint_PlayerInfo", lastPlayerState);
//         Debug.Log("Saving Player Info");
//     }

//     private void SetCurrentCheckpoint()
//     {
//         CurrentCheckpoint = transform;
//         if (_DisableCheckpointOnSet)
//         {
//             gameObject.SetActive(false);
//         }
//     }

//     public static void ReloadLastSavePoint(Transform playerTransform)
//     {
//         LoadPlayerData(playerTransform);
//         // LoadEnemiesData();
//     }

//     // private static void LoadEnemiesData()
//     // {
//     //     var lastEnemiesInfo = PlayerPrefs.GetString("SavePoint_EnemiesInfo");
//     //     var enemiesInfo = JsonUtility.FromJson<EnemiesContainer>(lastEnemiesInfo);

//     //     // Destroy all current enemies
//     //     for (int i = EnemyAttack.Enemies.Count - 1; i >= 0; i--)
//     //     {
//     //         Destroy(EnemyAttack.Enemies[i].gameObject);
//     //     }

//     //     foreach (var item in enemiesInfo.Enemies)
//     //     {
//     //         // TODO: Spawn new enemies bsed on this info
//     //     }
//     // }

//     private static void LoadPlayerData(Transform playerTransform)
//     {
//         var lastPlayerState = PlayerPrefs.GetString("SavePoint_PlayerInfo");
//         var playerData = JsonUtility.FromJson<PlayerData>(lastPlayerState);

//         //Update Current player info to playerData...
//         playerTransform.position = playerData.Position;
//         playerTransform.rotation = playerData.Rotation;
//     }

// }
