﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;

public class GameManager : MonoBehaviour
{
  [SerializeField]
  private float _endWaitTime = 5;
  
  [Header("Player")]
  [SerializeField]
  private RowingMovement _playerController;
  [SerializeField]
  private BoatHealth _playerHealth;
  [SerializeField]
  private RunesController _playerRunes;
  private Rigidbody _playerRB;

  [Header("Particles")]
  [SerializeField]
  private GameObject _lightningSprite;
  [SerializeField]
  private float _lightningLength = 0.1f;
  private Vector3 _verticalOffset = new Vector3(0,17.5f,0);

  [Header("Level")]
  [SerializeField]
  private GameObject _sunRotator;
  private Animator _sunAnimator;

  [SerializeField]
  private GameObject _endText;

  [SerializeField]
  private EventSystem _eventSystem;
  [SerializeField]
  private LevelTimer _levelTimer;

  [SerializeField]
  private GameObject _enemySpawner;
  
  [SerializeField]
  private Material _lighthouseON;
  [SerializeField]
  private Material _lighthouseOFF;
  [SerializeField]
  private MeshRenderer[] _lighthousesMeshRenderers;

  [Header("Lights")]
  [SerializeField]
  private Light[] _lvl1Lights;
  [SerializeField]
  private Light[] _lvl2Lights;
  [SerializeField]
  private Light[] _lvl3Lights;

  [Header("UI")]
  [SerializeField]
  private GameObject _pauseScreen;
  [SerializeField]
  private Button _pauseSelectedButton;
  private bool _isPaused = false;
  
  [SerializeField]
  private GameObject _shop;
  [SerializeField]
  private Button _buttonToSelect;
  [SerializeField]
  private Button _nextLevelButton;

  [SerializeField]
  private GameObject _deathScreen;
  [SerializeField]
  private Button _deathFirstButton;
  private bool _isPlayerDeath = false;

  [SerializeField]
  private Image _leftTrigger;
  [SerializeField]
  private Image _leftBumper;
  [SerializeField]
  private Image _rightTrigger;
  [SerializeField]
  private Image _rightBumper;
  [SerializeField]
  private Sprite _LT;
  [SerializeField]
  private Sprite _LB;
  [SerializeField]
  private Sprite _RT;
  [SerializeField]
  private Sprite _RB;

  private int _controlScheme;

  [Header("Areas")]
  private int _currentArea = 0;
  [SerializeField]
  private Transform[] _areas = new Transform[3];

  [Header("Music")]
  [SerializeField]
  private AudioSource _levelMusic;
  [SerializeField]
  private AudioClip _endSound;


  private void OnEnable()
  {
    _playerController.LevelCompleted += LevelComplete;
    _playerController.GamePaused += PauseGame;
    _playerHealth.PlayerDied += DeathScreen;
    _levelTimer.NightFall += StartEnemyWaves;
  }
  private void OnDisable()
  {
    _playerController.LevelCompleted -= LevelComplete;
    _playerController.GamePaused -= PauseGame;
    _playerHealth.PlayerDied -= DeathScreen;
    _levelTimer.NightFall -= StartEnemyWaves;
  }

  private void Awake()
  {
    _sunAnimator = _sunRotator.gameObject.GetComponent<Animator>();

    _playerRB = _playerController.GetComponentInParent<Rigidbody>();

    for (int i = 0; i < _lvl2Lights.Length; i++)
    {
      _lvl2Lights[i].gameObject.SetActive(false);
    }      
    for (int i = 0; i < _lvl3Lights.Length; i++)
    {
      _lvl3Lights[i].gameObject.SetActive(false);
    }
    _controlScheme =  PlayerPrefs.GetInt("Controls");

    if (_controlScheme == 1)
    {
      _leftTrigger.sprite = _LT;
      _leftBumper.sprite = _LB;
      _rightTrigger.sprite = _RT;
      _rightBumper.sprite = _RB;
    }
    else
    {
      _leftTrigger.sprite = _RT;
      _leftBumper.sprite = _RB;
      _rightTrigger.sprite = _LT;
      _rightBumper.sprite = _LB;
    }
  }
  public void PauseGame()
  {
    if (_isPaused == false)
    {
      Time.timeScale = 0; //Pause the time inside of the game      

      _pauseScreen.SetActive(true);
      if (EventSystem.current.currentSelectedGameObject != _pauseSelectedButton.gameObject)
      {
        _pauseSelectedButton.Select();
      }


      _isPaused = true;
    }
    else 
    {
      Time.timeScale = 1; //Resume the time inside of the game

      _pauseScreen.SetActive(false);


      _isPaused = false;
    }
  }

  public void DeathScreen()
  {
    if (_isPlayerDeath == false)
    {
      RowingMovement.TurnOffControl();

      _deathScreen.SetActive(true);
      _isPlayerDeath = true;  
      _deathFirstButton.Select();
      Debug.Log("Showing Death Screen");
    }
    else
    {
      _deathScreen.SetActive(false);
      _isPlayerDeath = false;
    }
  }

  private void LevelComplete()
  {
    //Pause Time
    Time.timeScale = 0;

    _playerController._atShop = true;

    _enemySpawner.GetComponent<EnemySpawner>().StopSpawning();
    _enemySpawner.GetComponent<EnemySpawner>().ResetCount();

    if (_currentArea == 0)
    {
      for (int i = 0; i < _lvl1Lights.Length; i++)
      {
        _lvl1Lights[i].gameObject.SetActive(false);
      }
      for (int i = 0; i < _lvl2Lights.Length; i++)
      {
        _lvl2Lights[i].gameObject.SetActive(true);
      }
      _lighthousesMeshRenderers[0].material = _lighthouseOFF;
      _lighthousesMeshRenderers[1].material = _lighthouseON;
    }
    else if (_currentArea == 1)
    {
      for (int i = 0; i < _lvl2Lights.Length; i++)
      {
        _lvl2Lights[i].gameObject.SetActive(false);
      }
      for (int i = 0; i < _lvl3Lights.Length; i++)
      {
        _lvl3Lights[i].gameObject.SetActive(true);
      }
      _lighthousesMeshRenderers[1].material = _lighthouseOFF;
      _lighthousesMeshRenderers[2].material = _lighthouseON;
    }
    
    if(_currentArea < 2)
    {
      //Fade out the music
      StartCoroutine(_levelMusic.gameObject.GetComponent<BeatSystem>().FadeOutAudio());

      LevelTimer.PauseTimer();
      //Update the player's current area
      _currentArea += 1;
      _shop.SetActive(true);
    }
    else
    {
      _endText.SetActive(true);
      StartCoroutine("GameFinished");
    }
  }

  private IEnumerator GameFinished()
  {
    _levelMusic.ignoreListenerPause = true;
    AudioListener.pause = true;
    _levelMusic.Stop();
    _levelMusic.clip = _endSound;
    _levelMusic.Play();
    yield return new WaitForSecondsRealtime(_endWaitTime);
    Time.timeScale = 1;
    SceneManager.LoadScene("Credits");
  }

  public void SelectButton()
  {
    // _eventSystem.SetSelectedGameObject(_buttonToSelect.gameObject);

    //Set Lightning Rune Button as Selected
    _buttonToSelect.Select();
  }

  public void NextLevel()
  {
    //Reset Timer
    _levelTimer.ResetTimer();
    _playerController.NewLevel();
    
    //Place Player at next Area
    _playerController.gameObject.transform.position = _areas[_currentArea].position;
    _playerController.gameObject.transform.rotation = _areas[_currentArea].rotation;

    _playerRB.velocity = Vector3.zero;
    _playerRB.angularVelocity = Vector3.zero;

    //Return Control to the player
    // RowingMovement.GiveControl();

    //Resume Time
    Time.timeScale = 1;
    // _sunAnimator.Play("DayCycle", 0, 1 - (Checkpoint.CheckpointTime/180));
    _sunAnimator.Play("DayCycle", 0, 0);

    _levelMusic.Play();
    _levelMusic.gameObject.GetComponent<BeatSystem>().RestartMusic();

    _playerController._atShop = false;
    _playerRunes.DoubleCheck();

    _levelMusic.GetComponent<BeatSystem>().EnableCountdown();
  }

  public void MainMenu()
  {
    BeatSystem._gamePaused = false;
    // RowingMovement.TurnOffControl();
    SceneManager.LoadScene(0);
    Time.timeScale = 1;
    RowingMovement.GiveControl();
  }

  public void QuitGame()
  {
    Application.Quit();
  }

  public void RestartLevel()
  {
    if (_isPaused == true)
    {
      PauseGame();
    }
    _sunAnimator.Play("DayCycle", 0, 0);

    _playerHealth.RestartLevel(_areas[_currentArea]);    
    _playerRunes.SwitchPause();
  }

  public void EnemyKilled(GameObject Enemy)
  {
    StartCoroutine(SpawnLightning(Enemy));
  }
  
  public IEnumerator SpawnLightning(GameObject Enemy)
  {
    float Timer = 0;

    GameObject _lightningBolt = Instantiate(_lightningSprite, Enemy.transform.position + _verticalOffset, Quaternion.Euler((Enemy.transform.position - _playerController.gameObject.transform.position)));
    _lightningBolt.transform.localScale = new Vector3(_lightningBolt.transform.localScale.x, 0, _lightningBolt.transform.localScale.z);
    
    while (Timer < _lightningLength)
    {
      // _lightningBolt.transform.LookAt(_playerController.gameObject.transform);
      _lightningBolt.transform.localScale = Vector3.Lerp(_lightningBolt.transform.localScale,  new Vector3(_lightningBolt.gameObject.transform.localScale.x, 4.2f, _lightningBolt.transform.localScale.z), Timer * 10);

      Timer += Time.deltaTime;

      Debug.Log("Running Coroutine");
      yield return null;
    }
    Destroy(_lightningBolt);
    Enemy.SetActive(false);
    yield return null;
  }

  private void StartEnemyWaves()
  {
    _enemySpawner.SetActive(true);
  }
}
