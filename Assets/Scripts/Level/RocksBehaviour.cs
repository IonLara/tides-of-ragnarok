﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RocksBehaviour : MonoBehaviour
{
    [SerializeField]
    private int _rockDamage = 1;

    [SerializeField]
    private float _damageSpeed = 8;

    [Header("Meta")]
    private int _playerLayerNumber = 10;

    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.layer == _playerLayerNumber)
        {
            if (other.relativeVelocity.magnitude >= _damageSpeed)
            {
                //Get Function from BoatHealth and Deal Damage
                other.gameObject.GetComponent<BoatHealth>().ReceiveDamage(_rockDamage, other.contacts[0].point, other.contacts[0].normal, 0);
            }
        }
    }
}
