﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class UISounds : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] _UISounds = new AudioClip[4];
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlayHighlightSound()
    {
        _audioSource.PlayOneShot(_UISounds[0]);
    }
    public void PlaySelectSound()
    {
        _audioSource.PlayOneShot(_UISounds[1]);
    }
    public void PlayReturnSound()
    {
        _audioSource.PlayOneShot(_UISounds[2]);
    }
    public void PlayGameStartSound()
    {
        _audioSource.PlayOneShot(_UISounds[3]);
    }
}