﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class BeatLine : MonoBehaviour
{
    private float _songBPM;
    private float _currentBPM;
    private float _secPerBeat;
    private float _songPosition;
    private float _songPositionInBeats;
    private float _dspSongTime;

    private float _beatIndex = 0;

    private Vector3 _spawnedPosition;
    private Vector3 _targetPosition = new Vector3 (0,0,0);

    private RectTransform _rectTransform;

    private int _direction;

    private void OnEnable()
    {
        _dspSongTime = BeatSystem._dspSongTime;
        _songBPM = BeatSystem._currentBPM;
        _rectTransform = GetComponent<RectTransform>();
        _direction = BeatSystem._lineDirection;

        _beatIndex = BeatSystem._beatNextIndex;
        
        _secPerBeat = 60f / _songBPM;

        _spawnedPosition = transform.position;
    }

    private void Update()
    {
        if (BeatSystem._gamePaused == false && BeatSystem._isAtShop == false)
        {
            _songPosition = (float)(AudioSettings.dspTime - _dspSongTime);
            _songPositionInBeats = _songPosition / _secPerBeat;

            _targetPosition.x = Mathf.Lerp(0, 350 * _direction, (2 - ((_beatIndex) - _songPositionInBeats)) / 2);
            
            _rectTransform.anchoredPosition = _targetPosition;

            if (_rectTransform.anchoredPosition.x == 350 || _rectTransform.anchoredPosition.x == -350)
            {
                // gameObject.SetActive(false);
                Destroy(gameObject);
            }            
        }
    }

    
    // [SerializeField]
    // private GameObject _prefab;

    // [SerializeField]
    // private float _speedMultiplier = 0.5f;

    // [SerializeField]
    // private float _spawnCadenceMult = 1.5f;

    // [SerializeField]
    // private float _beatDuration = 1f;

    // private GameObject[] _spawnedLines = new GameObject[100];
    // private int _currentLine = 0;

    // private float _timer;
    // private Vector3 _position = new Vector3(0, 0, 0);

    // private void Awake()
    // {
    //     _position = transform.position;
    //     _timer = 0;
    //     _spawnedLines[_currentLine] = Instantiate(_prefab, Vector3.zero, Quaternion.identity);
    // }

    // private void Update()
    // {
    //     if (_timer <= 0)
    //     {
    //         _timer = _beatDuration;

    //         _currentLine += 1;

    //         StartCoroutine(SpawnBeatLineRight());
    //         StartCoroutine(SpawnBeatLineLeft());
    //     }

    //     _timer -= Time.deltaTime * _spawnCadenceMult;
    // }

    // IEnumerator SpawnBeatLineRight()
    // {
    //     GameObject _line = Instantiate(_prefab, Vector3.zero, Quaternion.identity, transform);
    //     float t = 0f;
    //     while (t < 1)
    //     {
    //         _position.x = (Mathf.Lerp(0, 350, t));

    //         _line.GetComponent<RectTransform>().anchoredPosition = _position;

    //         t += Time.deltaTime * _speedMultiplier;
    //         yield return null;
    //     }
    //     Destroy(_line);
    // }
    // IEnumerator SpawnBeatLineLeft()
    // {
    //     GameObject _line = Instantiate(_prefab, Vector3.zero, Quaternion.identity, transform);
    //     float t = 0f;
    //     while (t < 1)
    //     {
    //         _position.x = (Mathf.Lerp(0, -350, t));

    //         _line.GetComponent<RectTransform>().anchoredPosition = _position;

    //         t += Time.deltaTime * _speedMultiplier;
    //         yield return null;
    //     }
    //     Destroy(_line);
    // }
}
