﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameManager _gameManager;

    [Header("Shop Buttons")]
    [SerializeField]
    private Button _lightningButton;
    [SerializeField]
    private Button _iceButton;
    [SerializeField]
    private Button _healthButton;

    [SerializeField]
    private TextMeshProUGUI _lightText;
    [SerializeField]
    private TextMeshProUGUI _iceText;
    [SerializeField]
    private TextMeshProUGUI _healthText;

    [Header("Prices")]
    [SerializeField]
    private float _lightRunePrice = 6000f;
    [SerializeField]
    private float _iceRunePrice = 5000f;
    [SerializeField]
    private float _healthRunePrice = 4000f;

    [Header("Runes")]
    [SerializeField]
    private RunePickup _lightningRune;
    [SerializeField]
    private RunePickup _iceRune;
    [SerializeField]
    private RunePickup _healthRune;

    [Header("Player")]
    [SerializeField]
    private RowingMovement _playerGold;
    [SerializeField]
    private RunesController _playerRunes;

    private void SetDefaultButton()
    {
        _gameManager.SelectButton();
    }

    private void ChangeLevel()
    {
        _gameManager.NextLevel();
    }

    private void UnableSelf()
    {
        this.gameObject.SetActive(false);
    }

    //Function to check the Player's Gold and Runes Inventory and Disable the shop buttons accordingly
    public void CheckPlayerInventory()
    {
        //Gold
        //Check if player has minimum gold for buying Runes
        if (_playerGold._currentGold < _healthRunePrice)
        {
            _lightningButton.interactable = false;
            _iceButton.interactable = false;
            _healthButton.interactable = false;
        }
        //Check if player has minimum gold to buy the Ice and Lightning Runes
        else if (_playerGold._currentGold < _iceRunePrice)
        {
            _lightningButton.interactable = false;
            _iceButton.interactable = false;
        }
        //Check if player has minimum gold to buy Lightning Rune
        else if (_playerGold._currentGold < _lightRunePrice)
        {
            _lightningButton.interactable = false;
        }

        //Runes
        //Check if player has Lightning Rune in his inventory
        if (_playerRunes._hasLightRune == true)
        {
            _lightningButton.interactable = false;
            _lightText.text = "1/1";
            //Put here a notice of "Already on Inventory"
        }
        else
        {
            _lightText.text = "0/1";
        }
        //Check if player has Ice Rune in his Inventory
        if (_playerRunes._hasIceRune == true)
        {
            _iceButton.interactable = false;
            _iceText.text = "1/1";
            //Put here a notice of "Already on Inventory"
        }
        else
        {
            _iceText.text = "0/1";
        }
        //Check if player has Health Rune in his Inventory
        if (_playerRunes._hasHealthRune == true)
        {
            _healthButton.interactable = false;
            _healthText.text = "1/1";
            //Put here a notice of "Already on Inventory"
        }
        else
        {
            _healthText.text = "0/1";
        }
    }

    private void ResetStoreButtons()
    {
        _lightningButton.interactable = true;
        _iceButton.interactable = true;
        _healthButton.interactable = true;
    }

    public void BuyRune(int BoughtRuneIndex)
    {
        if (BoughtRuneIndex == 0)
        {
            _playerRunes.RuneBought(_lightningRune);
            _playerGold.ChangeCurrentGold(-_lightRunePrice);
        }
        else if (BoughtRuneIndex == 1)
        {
            _playerRunes.RuneBought(_iceRune);            
            _playerGold.ChangeCurrentGold(-_iceRunePrice);
        }
        else
        {
            _playerRunes.RuneBought(_healthRune);            
            _playerGold.ChangeCurrentGold(-_healthRunePrice);
        }
    }

    // private void ChangeButton(int currentIndex)
    // {
    //     if (currentIndex == 0)
    //     {
    //         if (_iceButton.interactable == true)
    //         {
    //             _iceButton.Select();
    //         }
    //         else if (_healthButton.interactable == true)
    //         {
    //             _healthButton.Select();
    //         }
    //         else
            
    //     }
    //     else if(currentIndex ==1)
    //     {

    //     }
    //     else
    //     {

    //     }
    // }
}
