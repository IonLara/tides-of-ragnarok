﻿using UnityEngine;
using TMPro;

public class GoldUI : MonoBehaviour
{
    [SerializeField]
    private RowingMovement _playerController;

    private TextMeshProUGUI _displayedText;

    private float _current = 0f;
    private float _next = 0f;
    private float _displayed = 0f;


    private float _lerp = 0;

    private bool _lerping = false;

    
    private void OnEnable()
    {
        _playerController.ChangeInGold += ChangeText;
    }
    private void OnDisable()
    {
        _playerController.ChangeInGold  -= ChangeText;
    }

    private void Awake()
    {
        _displayedText = gameObject.GetComponent<TextMeshProUGUI>();
    }
    
    void Update()
    {
        _displayed = Mathf.RoundToInt(_displayed);
        _displayedText.text = _displayed.ToString();

        if (_lerping == true)
        {
            _current = _displayed;
            _displayed = Mathf.Lerp(_current, _next, _lerp);

            _lerp += Time.fixedDeltaTime;
        }
        if (_lerp >= 1)
        {
            _lerping = false;
            _lerp = 0;
        }
    }

    private void ChangeText(float gold)
    {
        _next = gold;
        _lerping = true;
    }
}
