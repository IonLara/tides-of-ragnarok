﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class GammaSlider : MonoBehaviour
{
    private Slider _gammaSlider;

    [SerializeField]
    private PostProcessProfile _profile;

    private ColorGrading _colorGrading;

    private void Awake()
    {
        _gammaSlider = gameObject.GetComponent<Slider>();
        _gammaSlider.onValueChanged.AddListener(delegate {SetGamma(); });
        _colorGrading = _profile.GetSetting<ColorGrading>();
    }
    
    public void SetGamma()
    {
        
        _colorGrading.gamma.value = new Color(_gammaSlider.value,_gammaSlider.value,_gammaSlider.value,_gammaSlider.value);
    }
}
