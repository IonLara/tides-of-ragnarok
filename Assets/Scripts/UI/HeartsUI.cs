﻿using UnityEngine.UI;
using UnityEngine;

public class HeartsUI : MonoBehaviour
{
    private int _maxHealth = 5;
    private int _currentHealth = 5;
    private int _healthDifference = 0;

    [SerializeField]
    private Image[] _heartsImages;
    [SerializeField]
    private Sprite _fullHeart;
    [SerializeField]
    private Sprite _emptyHeart;

    [SerializeField]
    private BoatHealth _playerHealth;

    private void OnEnable()
    {
        _playerHealth.HealthChanged += ChangeHealth;
    }

    private void OnDisable()
    {
        _playerHealth.HealthChanged -= ChangeHealth;
    }

    private void ChangeHealth(int newHealth)
    {
        _currentHealth = newHealth;

        _healthDifference = _maxHealth - _currentHealth;

        if (_healthDifference > 0)
        {
            for (int i = 1; i < _healthDifference + 1; i++)
            {
                _heartsImages[_maxHealth - i].sprite = _emptyHeart;
            }
        }
        for (int i = 0; i < _currentHealth; i++)
        {
            _heartsImages[i].sprite = _fullHeart;
        }


    }
}
