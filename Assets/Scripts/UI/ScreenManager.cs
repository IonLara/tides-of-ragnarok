﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
    [SerializeField]
    private string _sceneToLoad;
    [SerializeField]
    private Slider _loadingbar;
    
    public void LoadAsync(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronous(sceneIndex));
    }

    public IEnumerator LoadAsynchronous (int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        _loadingbar.gameObject.SetActive(true);
        while (operation.isDone != true)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            _loadingbar.value = progress;

            yield return null;
        }
    }
    
    public void LoadScene()
    {
        SceneManager.LoadScene(_sceneToLoad);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
