﻿using UnityEngine;

public class BeatBarBehaviour : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField]
    private RowingMovement _RowingMovement;
    [SerializeField]
    private BeatSystem _BeatSystem;

    [Header("Feedback")]
    [SerializeField]
    private float _shakeSpeed = 0.5f;
    [SerializeField]
    private float _shakeAmount = 0.5f;

    [Header("Objects")]
    [SerializeField]
    private GameObject _beatLine;
    [SerializeField]
    private GameObject _rightDrumStick;
    [SerializeField]
    private GameObject _leftDrumStick;
    [SerializeField]
    private GameObject _drum;

    private AudioSource _audioSource;


    private float _beatLenght;

    private void Awake()
    {
        _beatLenght = _BeatSystem.BeatLenght();
        _audioSource = gameObject.GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _RowingMovement.RightRowAction += OnRightRow;
        _RowingMovement.LeftRowAction += OnLeftRow;
    }

    private void OnDisable()
    {

        _RowingMovement.RightRowAction -= OnRightRow;
        _RowingMovement.LeftRowAction -= OnLeftRow;
    }

    private void OnRightRow(float ID)
    {
        if (ID == 0) //Perfect Beat
        {
            PerfectBeat(_rightDrumStick);
        }
        else if (ID == 1) //Normal Beat
        {
            NormalBeat(_rightDrumStick);
        }
        else if (ID == 2) //Missed Beat
        {
            MissedBeat(_rightDrumStick);
        }
    }

    private void OnLeftRow(float ID)
    {
        if (ID == 0) //Perfect Beat
        {
            PerfectBeat(_leftDrumStick);
        }
        else if (ID == 1) //Normal Beat
        {
            NormalBeat(_leftDrumStick);
        }
        else if (ID == 2) //Missed Beat
        {
            MissedBeat(_leftDrumStick);
        }
    }

    void Update()
    {

    }

    private void MissedBeat(GameObject _sprite)
    {
        _sprite.gameObject.GetComponent<Animator>().SetTrigger("MissedBeat");
    }
    private void NormalBeat(GameObject _sprite)
    {
        _sprite.gameObject.GetComponent<Animator>().SetTrigger("NormalBeat");
    }
    private void PerfectBeat(GameObject _sprite)
    {
        _sprite.gameObject.GetComponent<Animator>().SetTrigger("PerfectBeat");
    }
}
