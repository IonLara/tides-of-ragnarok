﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VideoSettings : MonoBehaviour
{
    private Resolution[] _resolutions;

    [SerializeField]
    private TMP_Dropdown _resolutionDropdown;
    private int _currentResIndex = 0;

    private void Awake()
    {
        _resolutions = Screen.resolutions;
        
        _resolutionDropdown.ClearOptions();
        
        List<string> _availableResolutions = new List<string>();

        for (int i = 0; i < _resolutions.Length; i++)
        {
            string _option = _resolutions[i].width + " x " + _resolutions[i].height;
            _availableResolutions.Add(_option);
            
            if(_resolutions[i].width == Screen.currentResolution.width && _resolutions[i].height == Screen.currentResolution.height)
            {
                _currentResIndex = i;
            }
        }
        _resolutionDropdown.AddOptions(_availableResolutions);
        _resolutionDropdown.value = _currentResIndex;
        _resolutionDropdown.RefreshShownValue();
    }
   
    public void SetQuality(int Index)
    {
        QualitySettings.SetQualityLevel(Index);
    }

    public void SetResolution(int ResolutionIndex)
    {
        Resolution _targetResolution = _resolutions[ResolutionIndex];
        Screen.SetResolution(_targetResolution.width, _targetResolution.height, true);
    }
}
