﻿using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class SpinnerBehaviour : MonoBehaviour
{
    [SerializeField]
    private Button _optionsFirst;
    [SerializeField]
    private Button _menuFirst;
    [SerializeField]
    private CinemachineVirtualCamera _optionsCamera;

    private bool _onMenu = true;
    private bool _onOptions = false;
    private bool _onCredits = false;
    private bool _onSlider = false;

    private Animator _animator;

    private void Awake()
    {
        _animator = gameObject.GetComponent<Animator>();
    }

    private void ChangeToOptions()
    {
        _optionsFirst.Select();
        _onMenu = false;
        _onOptions = true;
    }
    private void ChangeToMenu()
    {
        _menuFirst.Select();
        _onMenu = true;
        _onOptions = false;
    }

    public void ChangingSlider()
    {
        if (_onSlider == false)
        {
            _onSlider = true;
        }
        else
        {
            _onSlider = false;
        }
    }

    public void BackToMenu()
    {
        _animator.SetTrigger("Menu");
        _optionsCamera.gameObject.SetActive(false);
        _onOptions = false;
        _onCredits = false;
        _onMenu = true;

        _menuFirst.Select();
    }
}
