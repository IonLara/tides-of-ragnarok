﻿using TMPro;
using UnityEngine;

public class ControlSettings : MonoBehaviour
{
    [SerializeField]
    private TMP_Dropdown _dropDown;

    private int _dropDownIndex;

    private void Awake()
    {
        _dropDownIndex = PlayerPrefs.GetInt("Controls");

        Debug.Log("Current Control Index is: " + _dropDownIndex);

        if (_dropDownIndex > 0)
        {
            _dropDown.value = 0;
        }
        else
        {
            _dropDown.value = 1;
        }
    }

    public void SetControlScheme(int Index)
    {
        if (Index == 0)
        {
            PlayerPrefs.SetInt("Controls", 1);
        }
        else
        {
            PlayerPrefs.SetInt("Controls", -1);
        }
    }
}
