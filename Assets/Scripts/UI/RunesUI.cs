﻿using UnityEngine;
using UnityEngine.UI;

public class RunesUI : MonoBehaviour
{
    [SerializeField]
    private RunesController _playerController;

    [Header("Sprites")]
    [SerializeField]
    private Sprite[] _sprites = new Sprite[6];
    [SerializeField]
    // private Color32 _iceRune;
    // [SerializeField]
    // private Color32 _healthRune;

    private Image _display;
    private int _displayIndex = 0;

    [SerializeField]
    private Image _previousRune;
    [SerializeField]
    private Image _nextRune;

    private void OnEnable()
    {
        _playerController.RuneChanged += ChangeDisplay;
        _playerController.UICheck += CheckSelected;
    }

    private void OnDisable()
    {
        _playerController.RuneChanged -= ChangeDisplay;
        _playerController.UICheck -= CheckSelected;
    }

    private void Awake()
    {
        _display = gameObject.GetComponent<Image>();
    }

    private void ChangeDisplay(int currentRune, bool available, bool previousAvailable, bool nextAvailable)
    {
        _displayIndex = currentRune;
        //Changing Sprite for selected Rune
        if (available == true)
        {
            _display.sprite = _sprites[currentRune];
        }
        else
        {
            _display.sprite = _sprites[currentRune + 3];
        }
        //If Equipped Rune is Lightning Rune
        if (_displayIndex == 0)
        {
            //Change Sprite for previous Rune
            if (previousAvailable == true)
            {
                //Health Rune
                _previousRune.sprite = _sprites[2];
            }
            else
            {
                //NO Health Rune
                _previousRune.sprite = _sprites[5];
            }
            //change Sprite for next Rune
            if (nextAvailable == true)
            {
                //Ice Rune
                _nextRune.sprite = _sprites[1];
            }
            else
            {
                //NO Ice Rune
                _nextRune.sprite = _sprites[4];
            }            
        }
        //If Equipped Rune is Ice Rune
        if (_displayIndex == 1)
        {
            //Change Sprite for previous Rune
            if (previousAvailable == true)
            {
                //Lightning Rune
                _previousRune.sprite = _sprites[0];
            }
            else
            {
                //NO Lightning Rune
                _previousRune.sprite = _sprites[3];
            }
            //change Sprite for next Rune
            if (nextAvailable == true)
            {
                //Health Rune
                _nextRune.sprite = _sprites[2];
            }
            else
            {
                //NO Health Rune
                _nextRune.sprite = _sprites[5];
            }            
        }
        //If Equipped Rune is Health Rune
        if (_displayIndex == 2)
        {
            //Change Sprite for previous Rune
            if (previousAvailable == true)
            {
                //Ice Rune
                _previousRune.sprite = _sprites[1];
            }
            else
            {
                //NO Ice Rune
                _previousRune.sprite = _sprites[4];
            }
            //change Sprite for next Rune
            if (nextAvailable == true)
            {
                //Lightning Rune
                _nextRune.sprite = _sprites[0];
            }
            else
            {
                //NO Lightning Rune
                _nextRune.sprite = _sprites[3];
            }            
        }
    }

    private void CheckSelected(int rune, bool action)
    {
        if (_displayIndex == rune)
        {
            if (action == true)
            {
                _display.sprite = _sprites[_displayIndex];
            }
            else
            {
                _display.sprite = _sprites[_displayIndex + 3];
            }
        }
    }
}
