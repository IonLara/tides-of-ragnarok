﻿using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class LevelComplete : MonoBehaviour
{
    [SerializeField]
    private RowingMovement _playerCharacter;

    [SerializeField]
    private string _displayedText = "Level Complete!  Press Escape to Exit";

    private TextMeshProUGUI _winText;

    private void OnEnable()
    {
        _playerCharacter.LevelCompleted += EndScreen;
    }

    private void OnDisable()
    {
        _playerCharacter.LevelCompleted -= EndScreen;
    }

    private void Awake()
    {
        _winText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    private void EndScreen()
    {
        _winText.text = _displayedText;
    }


}
