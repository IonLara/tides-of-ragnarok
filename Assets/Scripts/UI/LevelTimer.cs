﻿using System;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class LevelTimer : MonoBehaviour
{
    [SerializeField]
    private float _levelTime = 180;
    public static float Timer {get; private set;}

    private Animator _animator;

    private static bool _timerPaused = false;

    [SerializeField]
    private BoatHealth _boatHealth;

    public event Action NightFall;

    static private bool _isNight = false;

    private void Awake() 
    {
        Timer = _levelTime;

        _animator = gameObject.GetComponent<Animator>();
    }

    private void Update() 
    {

        if (_timerPaused == false)
        {
            if (Timer > 0)
            {
                Timer -= Time.deltaTime;

            }
        }
        
        if (Timer <= 0 && _isNight == false)
        {
            //Set here the monster spawning at the end of the time.
            NightFall.Invoke();

            _isNight = true;
        }
    }

    public static void SetTimer()
    {
        Timer = Checkpoint.CheckpointTime;

        if (_isNight == true)
        {
            _isNight = false;            
        }
    }

    public static void PauseTimer()
    {
        _timerPaused = true;
    }

    public void ResetTimer()
    {
        Timer = _levelTime;
        _animator.SetTrigger("Reset");
        _timerPaused = false;

        if (_isNight == true)
        {
            _isNight = false;            
        }
    }

}
