﻿using UnityEngine;

public class Goal : MonoBehaviour
{
    [SerializeField]
    private LayerMask _playerLayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == _playerLayer)
        {
            RowingMovement.TurnOffControl();
        }

    }   

}
