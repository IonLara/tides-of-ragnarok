﻿using UnityEngine;
using UnityEngine.Audio;

public class VolumeAdjusting : MonoBehaviour
{
    [SerializeField]
    private AudioMixer _masterMixer;
    [SerializeField]
    private AudioMixerGroup _musicMixer;
    [SerializeField]
    private AudioMixerGroup _SFXMixer;

    public void SetMasterVolume(float SliderValue)
    {
        _masterMixer.SetFloat("MasterVolume", Mathf.Log10(SliderValue) * 20);
    }

    public void SetMusicVolume(float SliderValue)
    {
        _masterMixer.SetFloat("MusicVolume", Mathf.Log10(SliderValue) * 20);        
    }

    public void SetSFXVolume(float SliderValue)
    {
        _masterMixer.SetFloat("SFXVolume", Mathf.Log10(SliderValue) * 20);
    }
}
