﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicBehaviour : MonoBehaviour
{
    [SerializeField]
    private AudioClip _musicLoop;
    [SerializeField]
    private AudioClip _musicIntro;
    [SerializeField]
    private AudioClip _shopMusicIntro;
    [SerializeField]
    private AudioClip _shopMusicLoop;
    [SerializeField]
    private AudioClip _shopMusicOutro;
    private AudioSource _audioSource;

    private bool _gamePaused = false;

    [SerializeField]
    private float _fadeTime;
    private float _startingVolume;

    [Header("Player")]
    [SerializeField]
    private RowingMovement _playerController;

    private void OnEnable()
    {
        _playerController.GamePaused += PauseMusic;
    }
    private void OnDisable()
    {
        _playerController.GamePaused -= PauseMusic;        
    }

    private void Awake()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();
        _startingVolume = _audioSource.volume;
    }

    void Update()
    {
        if (_audioSource.isPlaying == false && _gamePaused == false)
        {
            _audioSource.clip = _musicLoop;
            _audioSource.Play();
        }
    }

    public IEnumerator FadeOutAudio()
    {
        while (_audioSource.volume > 0)
        {
            _audioSource.volume -= _startingVolume * Time.fixedUnscaledDeltaTime / _fadeTime;

            Debug.Log("Music Volume is " + _audioSource.volume);

            yield return null;
        }
        _audioSource.Stop();
    }
    public void RestartMusic()
    {
        _audioSource.volume = _startingVolume;
    }

    public void PauseMusic()
    {
        if (_gamePaused == false)
        {
            _gamePaused = true;
            _audioSource.Pause();            
        }
        else
        {
            _audioSource.UnPause();
            _gamePaused = false;
        }
    }

}
