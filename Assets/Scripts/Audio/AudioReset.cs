﻿using UnityEngine;

public class AudioReset : MonoBehaviour
{
    private void Awake()
    {
        if (AudioListener.pause == true)
        {
            AudioListener.pause = false;
        }        
    }
}
