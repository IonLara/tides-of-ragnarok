﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
public class RunesController : MonoBehaviour
{
    [SerializeField]
    private int _runeLayer = 13;

    [SerializeField]
    private RowingMovement _playerGold;
    private BoatHealth _playerHealth;

    [Header("Controls")]
    [SerializeField]
    private float _dPadInputRange = 0.2f;

    [SerializeField]
    private int _selectedRune = 0;

    private bool _rightDPadInputRead = false;
    private bool _leftDPadInputRead = false;

    public event Action<int, bool, bool, bool> RuneChanged;

    private bool _dPadInputRead = false;

    private bool _success;

    private bool _isPaused;


    [Header("Sounds")]
    [SerializeField]
    private AudioSource _feedbackSource;
    [SerializeField]
    private AudioClip _runePickupSound;
    [SerializeField]
    private AudioClip[] _playerRunePickupSounds; 
    [SerializeField]
    private AudioClip _healthSound;
    [SerializeField]
    private AudioClip _lightningSound;
    [SerializeField]
    private AudioClip _iceSound;
    [SerializeField]
    private AudioClip _useRuneSound;
    [SerializeField]
    private AudioClip _changeSound;

    [SerializeField]
    private AudioClip _goldSound;

    [SerializeField]
    private AudioClip[] _playerHealedSounds;

    [Header("Inventory")]
    private RunePickup _lightRune = null;
    public bool _hasLightRune { get; private set;} = false;
    private RunePickup _iceRune = null;
    public bool _hasIceRune { get; private set;} = false;
    private RunePickup _healthRune = null;
    public bool _hasHealthRune { get; private set;} = false;

    public event Action<int, bool> UICheck;

    [Header("Effects")]
    [SerializeField]
    private GameObject _pickupParticles;
    [SerializeField]
    private GameObject _lightningArea;
    [SerializeField]
    private GameObject _iceArea;

    [SerializeField]
    private GameObject _goldParticles;

    [Header("UI")]
    [SerializeField]
    private Image _yIcon;
    [SerializeField]
    private Color _noAlpha;
    private Color _fullAlpha = Color.white;
    [SerializeField]
    private LayerMask _enemyLayer;
    [SerializeField]
    private Transform _particleTransform;
    [SerializeField]
    private GameObject _uiParticles;

    private void Awake()
    {
        _playerHealth = gameObject.GetComponent<BoatHealth>();
    }

    private void OnEnable()
    {
        _playerGold.GamePaused += SwitchPause;
        _playerGold.LevelCompleted += SwitchPause;
        _playerHealth.PlayerDied += SwitchPause;
    }
    private void OnDisable()
    {
        _playerGold.GamePaused -= SwitchPause;
        _playerGold.LevelCompleted -= SwitchPause;
        _playerHealth.PlayerDied -= SwitchPause;
    }

    private void Start()
    {
        UICheck.Invoke(0, false);

    }

    void Update()
    {
        if (_selectedRune == 0 && _hasLightRune == true)
        {
            _lightningArea.SetActive(true);
        }
        else
        {
            _lightningArea.SetActive(false);
        }
        if (_selectedRune == 1 && _hasIceRune == true)
        {
            _iceArea.SetActive(true);
        }
        else
        {
            _iceArea.SetActive(false);
        }
        
        
        Collider[] _overlappedEnemies = Physics.OverlapSphere(transform.position, 20, _enemyLayer);
        if (_overlappedEnemies.Length < 1 || _overlappedEnemies == null)
        {
            if (_selectedRune < 2)
            {
                _yIcon.color = _noAlpha;
            }
            else
            {
                if (_hasHealthRune == true && _playerHealth._boatHealth != _playerHealth._maxHealth)
                {
                    _yIcon.color = _fullAlpha;                    
                }
                else
                {
                    _yIcon.color = _noAlpha;
                }
            }
        }
        else
        {
            if(_selectedRune == 0)
            {
                if (_hasLightRune == true)
                {
                    _yIcon.color = _fullAlpha;
                }
                else
                {
                    _yIcon.color = _noAlpha;
                }
            }
            else if(_selectedRune == 1)
            {
                if (_hasIceRune == true)
                {
                    _yIcon.color = _fullAlpha;
                }
                else
                {
                    _yIcon.color = _noAlpha;
                }
            }
            if(_selectedRune == 2)
            {
                if (_hasHealthRune == true)
                {
                    _yIcon.color = _fullAlpha;
                }
                else
                {
                    _yIcon.color = _noAlpha;
                }
            }
        }
        
        if (_isPaused == false)
        {
            //Using the runes
            if (Input.GetButtonDown("useRune"))
            {
                if (_selectedRune == 0)
                {
                    if (_lightRune != null)
                    {
                        UseRune(_lightRune);

                        if (_success == true)
                        {
                            UICheck.Invoke(0, false);
                            _lightRune = null;
                            _hasLightRune = false;

                            _feedbackSource.PlayOneShot(_lightningSound);
                        }
                    }
                    else
                    {
                        Debug.Log("No Lightning Rune");
                    }
                }
                else if (_selectedRune == 1)
                {
                    if (_iceRune != null)
                    {
                        UseRune(_iceRune);

                        if (_success == true)
                        {
                            UICheck.Invoke(1, false);
                            _iceRune = null;
                            _hasIceRune = false;

                            _feedbackSource.PlayOneShot(_iceSound);
                        }
                    }
                    else
                    {
                        Debug.Log("No Ice Rune");
                    }
                }
                else if (_selectedRune == 2)//Use Health Rune
                {
                    if (_healthRune != null)
                    {
                        if (_playerHealth._boatHealth != _playerHealth._maxHealth)
                        {
                            UseRune(_healthRune);
                            _healthRune = null;
                            _hasHealthRune = false;
                            UICheck.Invoke(2, false);

                            _feedbackSource.PlayOneShot(_healthSound);

                            int _clipIndex = UnityEngine.Random.Range(0, _playerHealedSounds.Length);
                            _feedbackSource.clip = _playerHealedSounds[_clipIndex];
                            _feedbackSource.PlayDelayed(1);
                        }

                    }
                    else
                    {
                        Debug.Log("No Health Rune");
                    }
                }
            }

            //Changing equipped rune Right
            if (Input.GetAxis("changeRune") > _dPadInputRange)
            {
                if (_dPadInputRead == false)
                {
                    _dPadInputRead = true;

                    ChangeRuneRight(_selectedRune);
                    Debug.Log("Changed to rune " + _selectedRune);

                    SendEvent(_selectedRune);
                    _feedbackSource.PlayOneShot(_changeSound);
                    return;
                }
            }

            if (Input.GetAxis("changeRune") < -_dPadInputRange)
            {
                if (_dPadInputRead == false)
                {
                    _dPadInputRead = true;

                    ChangeRuneLeft(_selectedRune);
                    Debug.Log("Changed to rune " + _selectedRune);

                    SendEvent(_selectedRune);
                    _feedbackSource.PlayOneShot(_changeSound);
                    return;
                }
            }
            if (_dPadInputRead == true && Input.GetAxis("changeRune") <= _dPadInputRange && Input.GetAxis("changeRune") >= -_dPadInputRange)
            {
                _dPadInputRead = false;
            }
        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == _runeLayer)
        {
            RunePickup _pickedUpRune = other.GetComponent<RunePickup>();
            if (_pickedUpRune != null)
            {
                //Play UI Sound of picking up Rune
                _feedbackSource.PlayOneShot(_runePickupSound);
                int _clipIndex = UnityEngine.Random.Range(0, _playerRunePickupSounds.Length);
                //Play voice of characters finding rune
                _feedbackSource.clip = (_playerRunePickupSounds[_clipIndex]);
                _feedbackSource.PlayDelayed(0.5f);

                //Could do it this way if Type Checking doesn't work
                //if(pickedUpRune.runeType == RunePickup.RuneType.Lightning)

                if (_pickedUpRune._runeType.GetType() == typeof(LightningRune))
                {
                    if (_lightRune == null)
                    {
                        _lightRune = _pickedUpRune;
                        _pickedUpRune = null;
                        _hasLightRune = true;

                        Instantiate(_pickupParticles, other.gameObject.transform.position, other.gameObject.transform.rotation);
                        Instantiate(_uiParticles, _particleTransform);

                        other.gameObject.SetActive(false);

                        _selectedRune = 0;
                        SendEvent(_selectedRune);
                        
                        UICheck.Invoke(0, true);
                    }
                    else
                    {
                        _pickedUpRune = null;
                        Instantiate(_goldParticles, other.transform.position, other.transform.rotation);
                        _feedbackSource.PlayOneShot(_goldSound);
                        other.gameObject.SetActive(false);

                        _playerGold.ChangeCurrentGold(1500f);
                    }
                }
                else if (_pickedUpRune._runeType.GetType() == typeof(IceRune))
                {
                    if (_iceRune == null)
                    {
                        _iceRune = _pickedUpRune;
                        _pickedUpRune = null;
                        _hasIceRune = true;

                        Instantiate(_pickupParticles, other.gameObject.transform.position, other.gameObject.transform.rotation);
                        Instantiate(_uiParticles, _particleTransform);

                        other.gameObject.SetActive(false);
                        Debug.Log("Picked up Ice Rune");

                        _selectedRune = 1;
                        SendEvent(_selectedRune);

                        UICheck.Invoke(1, true);
                    }
                    else
                    {
                        _pickedUpRune = null;
                        other.gameObject.SetActive(false);
                        _playerGold.ChangeCurrentGold(1250);
                    }
                }
                else if (_pickedUpRune._runeType.GetType() == typeof(HealthRune))
                {
                    if (_healthRune == null)
                    {
                        _healthRune = _pickedUpRune;
                        _pickedUpRune = null;
                        _hasHealthRune = true;

                        Instantiate(_pickupParticles, other.gameObject.transform.position, other.gameObject.transform.rotation);
                        Instantiate(_uiParticles, _particleTransform);
                        
                        other.gameObject.SetActive(false);
                        Debug.Log("Picked up Health Rune");
                                             
                        _selectedRune = 2;
                        SendEvent(_selectedRune);
                        
                        UICheck.Invoke(2, true);
                    }
                    else
                    {
                        _pickedUpRune = null;
                        other.gameObject.SetActive(false);
                        _playerGold.ChangeCurrentGold(1000);
                    }
                }
            }

        }
    }

    private void UseRune(RunePickup rune)
    {
        _success = rune._runeType.ActivateRune(transform);
    }

    private void ChangeRuneRight(int currentRune)
    {
        if (currentRune != 2)
        {
            _selectedRune += 1;
        }
        else
        {
            _selectedRune = 0;
        }
    }
    private void ChangeRuneLeft(int currentRune)
    {
        if (currentRune == 0)
        {
            _selectedRune = 2;
        }
        else
        {
            _selectedRune -= 1;
        }
    }

    private void SendEvent(int rune)
    {
        if (rune == 0)
        {
            if (_lightRune != null)
            {
                RuneChanged.Invoke(_selectedRune, true, _hasHealthRune, _hasIceRune);
            }
            else
            {
                RuneChanged.Invoke(_selectedRune, false, _hasHealthRune, _hasIceRune);
            }
        }
        else if (rune == 1)
        {
            if (_iceRune != null)
            {
                RuneChanged.Invoke(_selectedRune, true, _hasLightRune, _hasHealthRune);
            }
            else
            {
                RuneChanged.Invoke(_selectedRune, false, _hasLightRune, _hasHealthRune);
            }
        }
        else
        {
            if (_healthRune != null)
            {
                RuneChanged.Invoke(_selectedRune, true, _hasIceRune, _hasLightRune);
            }
            else
            {
                RuneChanged.Invoke(_selectedRune, false, _hasIceRune, _hasLightRune);
            }
        }
    }
    public void RuneBought(RunePickup BoughtItem)
    {
        if (BoughtItem._runeType.GetType() == typeof(LightningRune))
        {
            _feedbackSource.PlayOneShot(_runePickupSound);

            _lightRune = BoughtItem;
            _hasLightRune = true;

            _selectedRune = 0;

            Debug.Log("Bought Lightning Rune");

            RuneChanged.Invoke(_selectedRune, true, _hasHealthRune, _hasIceRune);
            // UICheck.Invoke(0, true);
        }
    
        else if (BoughtItem._runeType.GetType() == typeof(IceRune))
        {
            _feedbackSource.PlayOneShot(_runePickupSound);

            _iceRune = BoughtItem;
            _hasIceRune = true;

            _selectedRune = 1;

            Debug.Log("Bought Ice Rune");

            RuneChanged.Invoke(_selectedRune, true, _hasLightRune, _hasHealthRune);
            UICheck.Invoke(1, true);
        }
    
        else if (BoughtItem._runeType.GetType() == typeof(HealthRune))
        {
            _feedbackSource.PlayOneShot(_runePickupSound);

            _healthRune = BoughtItem;
            _hasHealthRune = true;

            _selectedRune = 2;

            Debug.Log("Bought Health Rune");

            RuneChanged.Invoke(_selectedRune, true, _hasIceRune, _hasLightRune);
            UICheck.Invoke(2, true);
        }
    }

    public void SwitchPause()
    {
        if (_isPaused == true)
        {
            _isPaused = false;
        }
        else
        {
            _isPaused = true;
        }
    }
    public void DoubleCheck()
    {
        if (_isPaused == true)
        {
            _isPaused = false;
        }
    }
}

