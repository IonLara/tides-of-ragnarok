﻿using UnityEngine;

public class RowParticles : MonoBehaviour
{
    [SerializeField]
    private Transform _leftRowPosition;
    [SerializeField]
    private Transform _rightRowPosition;
    [SerializeField]
    private GameObject _particles;
    [SerializeField]
    private RowingMovement _playerControl;

    private void OnEnable()
    {
        // _playerControl.RightRowAction += SpawnRight;
        // _playerControl.LeftRowAction += SpawnLeft;
    }

    private void SpawnRight(float value)
    {
        if (_playerControl.fall == false)
        {
            Instantiate(_particles, _rightRowPosition);
        }
    }
    private void SpawnLeft(float value)
    {
        if (_playerControl.fall == false)
        {
            Instantiate(_particles, _leftRowPosition);
        }
    }
}
