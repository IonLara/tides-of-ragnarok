﻿using System;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class RowingMovement : MonoBehaviour
{
    private Rigidbody _rigidBody;

    private static bool _controlON = false;

    [Header("Animation")]
    [SerializeField]
    private Animator _animator;

    private float _currentAnimX;
    private float _currentAnimY;

    [Header("Beat")]
    [SerializeField]
    private BeatSystem _beatSystem;

    [Header("Movement")]
    [SerializeField]
    private float _force = 100f;
    [SerializeField]
    private float _torque = 100f;

    [SerializeField]
    private float _multiplier;

    private float _accuracyID = 0;

    private Vector2 _vectorID = new Vector2();

    public  float _currentGold { get; private set;}= 0;

    public event Action<float> ChangeInGold;

    //1 = Normal & -1 = Teachers
    private int _controlSchemeIndex = 1;

    [Header("Particles")]
    [SerializeField]
    private GameObject[] _movementParticles;

    [Header("Audio")]
    [SerializeField]
    private AudioClip[] _effortSounds = new AudioClip[8];
    // [SerializeField]
    // private float _effortVolume = 1f;
    // [SerializeField]
    // private AudioClip[] _rowingSounds = new AudioClip[3];
    // [SerializeField]
    // private float _splashVolume = 1f;

    [SerializeField]
    private AudioClip[] _fallingSounds;
    private bool _falling = false;

    [SerializeField]
    private AudioSource _effortSource;
    [SerializeField]
    private AudioSource _splashSource;

    [Header("Controls")]
    [SerializeField]
    private float _triggerInputRange = 0.2f;
    [SerializeField]
    private float _cooldown = 0.5f;

    //Forward Rowing Cooldown Variables
    private bool _leftRowCooling = false;
    private bool _rightRowCooling = false;
    private float _rightRowCoolDown;
    private float _leftRowCoolDown;

    //Back Rowing Cooldown Variables
    private bool _leftBackRowCooling = false;
    private bool _rightBackRowCooling = false;
    private float _leftBackRowCoolDown;
    private float _rightBackRowCoolDown;
    [SerializeField]
    private float _alphaDuration = 0.25f;
    [SerializeField]
    private Color _lowAlphaColor;
    private float _leftTriggerAlpha = 1f;
    private float _rightTriggerAlpha = 1f;
    private float _leftBumperAlpha = 1f;
    private float _rightBumperAlpha = 1f;

    public event Action<float> RightRowAction;
    public event Action<float> LeftRowAction;
    
    public event Action GamePaused;

    public bool _atShop = false;
    public bool _atDeathScreen = false;

    //Setting Bools to make Triggers act as Buttons
    private bool _rightTriggerInputRead = false;
    private bool _leftTriggerInputRead = false;

    [SerializeField]
    private int _goalLayer = 14;

    private bool _levelComplete = false;

    public event Action LevelCompleted;

    [Header("Camera")]
    [SerializeField]
    private CinemachineVirtualCamera _fallCam;

    [Header("UI")]
    [SerializeField]
    private Transform _leftPerfectBeatTransform;
    [SerializeField]
    private Transform _middlePerfectBeatTransform;
    [SerializeField]
    private Transform _rightPerfectBeatTransform;
    [SerializeField]
    private GameObject _perfectBeatParticles;

    [SerializeField]
    private Image _leftTriggerImage;
    [SerializeField]
    private Image _leftBumperImage;
    [SerializeField]
    private Image _rightTriggerImage;
    [SerializeField]
    private Image _rightBumperImage;

    private void Awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody>();
        _splashSource = gameObject.GetComponent<AudioSource>();
        _controlON = false;
    }
    private void Start()
    {
        ChangeInGold.Invoke(_currentGold);

        _controlSchemeIndex = PlayerPrefs.GetInt("Controls");
        Debug.Log("Index is : " + _controlSchemeIndex);
    }

    void Update()
    {
        GamePause();

        DebugTools();

        //Play Falling sound when player starts falling from world
        if (_falling == false && fall == true)
        {
            int _clipIndex = UnityEngine.Random.Range(0, _fallingSounds.Length);
            _effortSource.PlayOneShot(_fallingSounds[_clipIndex]);
            _falling = true;
        }

        InputCooldowns();
        InputControl();

        InputAlphaControl();

    }

    private void InputAlphaControl()
    {
        _leftTriggerAlpha += Time.deltaTime / _alphaDuration;
        _rightTriggerAlpha += Time.deltaTime / _alphaDuration;
        _leftBumperAlpha += Time.deltaTime / _alphaDuration;
        _rightBumperAlpha += Time.deltaTime / _alphaDuration;

        if(_leftTriggerAlpha > 1f) _leftTriggerAlpha = 1f;
        if(_rightTriggerAlpha > 1f) _rightTriggerAlpha = 1f;

        _leftTriggerImage.color = Color.Lerp(_lowAlphaColor, Color.white, _leftTriggerAlpha);
        _leftBumperImage.color = Color.Lerp(_lowAlphaColor, Color.white, _leftBumperAlpha);
        _rightTriggerImage.color = Color.Lerp(_lowAlphaColor, Color.white, _rightTriggerAlpha);
        _rightBumperImage.color = Color.Lerp(_lowAlphaColor, Color.white, _rightBumperAlpha);
    }


    private void InputControl()
    {
        if (_controlON == true)
        {

            //Forward Rowing Right
            if (Input.GetAxis("axisRowing_Right") > _triggerInputRange && _rightRowCooling == false)
            // if (Input.GetButtonDown("axisRowing_Right")  && _rightRowCooling == false)
            {
                if (_rightTriggerInputRead == false)
                {
                    _rightTriggerInputRead = true;

                    _accuracyID = Row(1f * _controlSchemeIndex);

                    RightRowAction.Invoke(_accuracyID);

                    _animator.SetTrigger("RowRight");

                    if (_accuracyID == 0)
                    {
                        Instantiate(_perfectBeatParticles, _middlePerfectBeatTransform);
                    }

                    _rightRowCoolDown = _cooldown;
                    _rightRowCooling = true;

                    if(_controlSchemeIndex == 1)
                    {
                        _rightTriggerAlpha = 0f;
                    }
                    else
                    {
                        _leftTriggerAlpha = 0f;
                    }
                }
            }
            if (Input.GetAxis("axisRowing_Right") < _triggerInputRange)
            // if (Input.GetButtonDown("axisRowing_Right"))
            {
                if (_rightTriggerInputRead == true)
                {
                    _rightTriggerInputRead = false;
                }
            }
            //Forward Rowing Left
            if (Input.GetAxis("axisRowing_Left") > _triggerInputRange && _leftRowCooling == false)
            {
                if (_leftTriggerInputRead == false)
                {
                    _leftTriggerInputRead = true;

                    _accuracyID = Row(-1f * _controlSchemeIndex);

                    LeftRowAction.Invoke(_accuracyID);

                    _animator.SetTrigger("RowLeft");

                    if (_accuracyID == 0)
                    {
                        Instantiate(_perfectBeatParticles, _middlePerfectBeatTransform);
                    }

                    _leftRowCoolDown = _cooldown;
                    _leftRowCooling = true;

                    if(_controlSchemeIndex == 1)
                    {
                        _leftTriggerAlpha = 0f;
                    }
                    else
                    {
                        _rightTriggerAlpha = 0f;
                    }
                }
            }
            if (Input.GetAxis("axisRowing_Left") < _triggerInputRange)
            {
                if (_leftTriggerInputRead)
                {
                    _leftTriggerInputRead = false;
                }
            }

            //Backwards Rowing Right
            if (Input.GetButtonDown("RowRight") && _rightBackRowCooling == false)
            {
                // Debug.Log("Rowing Right");

                _accuracyID = RowBack(-1f * _controlSchemeIndex);

                RightRowAction.Invoke(_accuracyID);

                _animator.SetTrigger("RowLeftBack");

                if (_accuracyID == 0)
                {
                    Instantiate(_perfectBeatParticles, _middlePerfectBeatTransform);
                }

                _rightBackRowCoolDown = _cooldown;
                _rightBackRowCooling = true;

                if(_controlSchemeIndex == 1)
                {
                    _rightBumperAlpha = 0f;
                }
                else
                {
                    _leftBumperAlpha = 0f;
                }
            }
            //Backwards Rowing Left
            if (Input.GetButtonDown("RowLeft") && _leftBackRowCooling == false)
            {
                // Debug.Log("Rowing Left");

                _accuracyID = RowBack(1f * _controlSchemeIndex);

                _animator.SetTrigger("RowRightBack");

                LeftRowAction.Invoke(_accuracyID);

                if (_accuracyID == 0)
                {
                    Instantiate(_perfectBeatParticles, _middlePerfectBeatTransform);
                }

                _leftBackRowCoolDown = _cooldown;
                _leftBackRowCooling = true;

                if(_controlSchemeIndex == 1)
                {
                    _leftBumperAlpha = 0f;
                }
                else
                {
                    _rightBumperAlpha = 0f;
                }
            }

        }
    }


    private void InputCooldowns()
    {
        //Reset Left Row Cooldown
        if (_leftRowCoolDown <= 0 && _leftRowCooling == true)
        {
            _leftRowCooling = false;
        }
        //Reset Right Row Cooldown
        if (_rightRowCoolDown <= 0 && _rightRowCooling == true)
        {
            _rightRowCooling = false;
        }
        //Reset Left Back Row Cooldown
        if (_leftBackRowCoolDown <= 0 && _leftBackRowCooling == true)
        {
            _leftBackRowCooling = false;
        }
        //Reset Right Back Row Cooldown
        if (_rightBackRowCoolDown <= 0 && _rightBackRowCooling == true)
        {
            _rightBackRowCooling = false;
        }

        //Cooldown for Left Rowing
        if (_leftRowCoolDown > 0)
        {
            _leftRowCoolDown -= Time.deltaTime;
        }
        //Cooldown for Right Rowing
        if (_rightRowCoolDown > 0)
        {
            _rightRowCoolDown -= Time.deltaTime;
        }
        //Cooldown for Left Back Rowing
        if (_leftBackRowCoolDown > 0)
        {
            // Debug.Log($"LeftRowCoolDown : [{_leftBackRowCoolDown}]");
            _leftBackRowCoolDown -= Time.deltaTime;
        }
        //Cooldown for Right Back Rowing
        if (_rightBackRowCoolDown > 0)
        {
            _rightBackRowCoolDown -= Time.deltaTime;
        }
    }

    private void GamePause()
    {
        //Pausing Game
        if (_atShop == false && _atDeathScreen == false)
        {
            if (Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Escape))
            {
                GamePaused.Invoke();
            }
        }
    }

    private void DebugTools()
    {
        //Debug Tools
        //Increase Time Scale
        if (Input.GetKeyDown(KeyCode.T))
        {
            Time.timeScale = 3;
        }
        //Extreme Fast Forward
        if (Input.GetKeyDown(KeyCode.Y))
        {
            Time.timeScale = 15;
        }
        //Reset Time Scale
        if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            _controlON = false;

            LevelCompleted.Invoke();

            _levelComplete = true;
        }
    }

    public LayerMask waterLayerMask = 0;
    public bool fall = false;
    public AQUAS_Buoyancy bouyancy = null;

    private void FixedUpdate()
    {
        Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);

        if (fall == true && Physics.Raycast(ray, 3f, waterLayerMask) == true)
        {
            bouyancy.enabled = true;
            fall = false;
            _falling = false;
            _fallCam.Priority = 9;

            for (int i = 0; i < _movementParticles.Length; i++)
            {
                _movementParticles[i].SetActive(true);
            }
        }
        if (Physics.Raycast(ray, 3f, waterLayerMask) == false)
        {
            bouyancy.enabled = false;
            fall = true;

            _fallCam.Priority = 11;

            for (int i = 0; i < _movementParticles.Length; i++)
            {
                _movementParticles[i].SetActive(false);
            }
        }
        Vector3 _clamppedVelocity = _rigidBody.velocity;
        if (fall == false)
        {
            _clamppedVelocity.y = 0;
            _rigidBody.velocity = _clamppedVelocity;
        }


        Vector3 _clamppedRotation = _rigidBody.rotation.eulerAngles;
        _clamppedRotation.x = 0;
        _clamppedRotation.z = 0;
        _rigidBody.rotation = Quaternion.Euler(_clamppedRotation);
    }

    public static void TurnOffControl()
    {
        _controlON = false;
    }

    public float Row(float direction)
    {
        _vectorID = _beatSystem.GetBeatTime();

        _rigidBody.AddForce(transform.forward * _force * _vectorID.x, ForceMode.Force);
        _rigidBody.AddTorque(transform.up * _torque * _vectorID.x * direction, ForceMode.Force);

        int _clipIndex = UnityEngine.Random.Range(0, 7);
        // bool _isPlaying = _effortSource.isPlaying;
        _effortSource.PlayOneShot(_effortSounds[_clipIndex]);
        // _clipIndex = UnityEngine.Random.Range(0, _rowingSounds.Length-1);
        // _splashSource.PlayOneShot(_rowingSounds[_clipIndex]);

        return _vectorID.y;
    }
    public float RowBack(float direction)
    {
        _vectorID = _beatSystem.GetBeatTime();

        _rigidBody.AddForce(transform.forward * _force * -1 * _vectorID.x, ForceMode.Force);
        _rigidBody.AddTorque((transform.up * _torque * direction), ForceMode.Force);

        //Play the Audio
        int _clipIndex = UnityEngine.Random.Range(0, 7);
        _effortSource.PlayOneShot(_effortSounds[_clipIndex]);
        // _clipIndex = UnityEngine.Random.Range(0, _rowingSounds.Length);
        // _splashSource.PlayOneShot(_rowingSounds[_clipIndex]);

        return _vectorID.y;
    }

    private void OnTriggerEnter(Collider other)
    //Finishing a Level
    {
        if (other.gameObject.layer == _goalLayer && _levelComplete == false)
        {
            _controlON = false;

            LevelCompleted.Invoke();

            _levelComplete = true;
        }
    }
    public static void GiveControl()
    {
        _controlON = true;
    }

    public void ChangeCurrentGold(float gold)
    {
        _currentGold += gold;
        Debug.Log("You have " + _currentGold + " gold coins!");

        ChangeInGold.Invoke(_currentGold);
    }

    public void NewLevel()
    {
        _levelComplete = false;
    }
}
