﻿using System;
using System.Collections;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(Collider), typeof(Rigidbody), typeof(AudioSource))]
public class BoatHealth : MonoBehaviour
{
  [SerializeField]
  public int _maxHealth {get; private set;} = 5;
  public int _boatHealth {get; private set;}
  [SerializeField]
  private float _damageCooldownTime = 1;
  private float _timer= 0;

  [SerializeField]
  private float _bounceMagnitude = 1f;
  
  private Vector3 _bounceDirection = new Vector3();

  private Rigidbody _rigidBody;
  private Collider _collider;

  private RowingMovement _playerController;
  private RunesController _playerRunes;

  [SerializeField]
  private Animator _boatAnimator;

  [Header("World Animations")]
  [SerializeField]
  private Animator _timerAnimator;
  [SerializeField]
  private Animator _sunAnimator;

  [Header("Sound")]
  [SerializeField]
  private AudioSource _audioSource;
  [SerializeField]
  private AudioSource _voiceSource;
  [SerializeField]
  private AudioClip[] _hitClip;
  [SerializeField]
  private AudioClip[] _damageSounds;
  [SerializeField]
  private AudioClip _sinkSound;
  [SerializeField]
  private AudioClip[] _deathScreams;

  [Header("Particles")]
  [SerializeField]
  private GameObject _hitParticles;
  
  public event Action PlayerDied;
  private bool _isPlayerDead = false;
  public event Action<int> HealthChanged;
  private bool _canBeDamaged = true;
  public event Action PlayerRespawned;

  [Header("UI")]
  [SerializeField]
  private Transform[] _heartTransforms;
  [SerializeField]
  private GameObject _shardParticle;

  [Header("Timer")]
  [SerializeField]
  private LevelTimer _levelTimer;

  [Header("Camera")]
  [SerializeField]
  private CinemachineVirtualCamera _fallCam;
  
  private void Awake()
  {
    _boatHealth = _maxHealth;

    _rigidBody = gameObject.GetComponent<Rigidbody>();

    _playerController = gameObject.GetComponent<RowingMovement>();
    _playerRunes = gameObject.GetComponent<RunesController>();

    _collider = gameObject.GetComponent<Collider>();
  }
  
  private void Update()
  {
    //Debug Damage Tool
    if (Input.GetKeyDown(KeyCode.K))
    {
      ReceiveDamage(1, transform.position, transform.position, 1);
    }
    if (_timer > 0)
    {
      _timer -= Time.deltaTime;
    }
    if (_timer <= 0)
    {
      _canBeDamaged = true;
    }
  }
  private void FixedUpdate()
  {
    if(_isPlayerDead == true)
    {
      _rigidBody.isKinematic = true;
    }    
    else
    {
      _rigidBody.isKinematic = false;
    }  
  }


  public void ReceiveDamage(int damage, Vector3 contactPoint, Vector3 contactNormal, float collisionVelocity)
  {
    if (_canBeDamaged == true && _isPlayerDead == false)
    {
      _boatHealth -= damage;
      Instantiate(_shardParticle, _heartTransforms[_boatHealth]);

      _canBeDamaged = false;
      _timer = _damageCooldownTime;

      //Get Boat velocity magnitude
      var _velocity = _rigidBody.velocity.magnitude;
      _rigidBody.AddForce((transform.forward - contactNormal) * (_velocity + (collisionVelocity * 2)) * _bounceMagnitude, ForceMode.Force);

      int _clipIndex = UnityEngine.Random.Range(0, _hitClip.Length);
      _audioSource.PlayOneShot(_hitClip[_clipIndex]);

      _clipIndex = UnityEngine.Random.Range(0, _damageSounds.Length);
      _audioSource.PlayOneShot(_damageSounds[_clipIndex]);

      Instantiate(_hitParticles, contactPoint, Quaternion.Euler(contactPoint - transform.position));


      Debug.Log(damage + " Damage Taken");

      if (_boatHealth <= 0)
      {
        if(_isPlayerDead == false)
        {
          PlayerDied.Invoke();

          _playerController._atDeathScreen = true;

          _boatAnimator.SetTrigger("Death");
          _audioSource.PlayOneShot(_sinkSound);
          
          _clipIndex = UnityEngine.Random.Range(0, _deathScreams.Length);
          _voiceSource.PlayOneShot(_deathScreams[_clipIndex]);

          HealthChanged.Invoke(0);
          _isPlayerDead = true;
        }
      }
      else
      {
        HealthChanged.Invoke(_boatHealth);
      }
    }
    
  }

  public bool Heal(int healingPoints)
  {
    if (_boatHealth < _maxHealth)
    {
      _boatHealth += healingPoints;
      if (_boatHealth > _maxHealth)
      {
        _boatHealth = _maxHealth;
      }
      HealthChanged.Invoke(_boatHealth);

      return true;
    }
    else
    {
      return false;
    }
  }

  public void CheckPoint()
  {
    if (PlayerRespawned != null)
    {
      PlayerRespawned.Invoke();        
    }

    if (_fallCam.Priority > 10)
    {
        _fallCam.Priority = 9;
    }

    _boatAnimator.Play("Idle");
    
    _boatHealth = _maxHealth;

    // PlayerDied.Invoke();
    HealthChanged.Invoke(_boatHealth);
    LevelTimer.SetTimer();

    _timerAnimator.Play("Time", 0, 1 - (Checkpoint.CheckpointTime/180));
    _sunAnimator.Play("DayCycle", 0, 1 - (Checkpoint.CheckpointTime/180));

    transform.position = Checkpoint.CurrentCheckpoint.position;
    transform.rotation = Checkpoint.CurrentCheckpoint.rotation;

    _rigidBody.velocity = Vector3.zero;
    _rigidBody.angularVelocity = Vector3.zero;
    
    _isPlayerDead = false;
    _playerController._atDeathScreen = false;
    RowingMovement.GiveControl();
    _playerRunes.DoubleCheck();
  }

  public void RestartLevel(Transform AreaTransform)
  {
    if (PlayerRespawned != null)
    {
      PlayerRespawned.Invoke();        
    }

    _boatAnimator.Play("Idle");

    _boatHealth = _maxHealth;

    HealthChanged.Invoke(_boatHealth);
    _levelTimer.ResetTimer();
    _timerAnimator.Play("Time", 0, 0);

    transform.position = AreaTransform.position;
    transform.rotation = AreaTransform.rotation;
    _rigidBody.velocity = Vector3.zero;
    _rigidBody.angularVelocity = Vector3.zero;
    _isPlayerDead = false;
    _playerController._atDeathScreen = false;
    RowingMovement.GiveControl();
  }

  private IEnumerator DamageCooldown()
  {
    _canBeDamaged = false;
    yield return new WaitForSeconds(_damageCooldownTime);
    _canBeDamaged = true;
  }
}
