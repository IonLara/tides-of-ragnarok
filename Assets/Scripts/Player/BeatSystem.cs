﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(AudioSource))]
public class BeatSystem : MonoBehaviour
{
    [Header("Sound")]
    [SerializeField]
    private AudioClip[] _drumBeatSound = new AudioClip[2];
    [SerializeField]
    private float _volume = 0.7f;
    private int _clipIndex = 0;
    private AudioSource _audioSource;

    [Header("Timer")]
    [SerializeField]
    private float _beatLength = 0.5f;
    [SerializeField]
    private float _perfectBeat = 0.05f;
    [SerializeField]
    private float _normalBeat = 0.1f;

    [Header("Multiplier")]
    [SerializeField]
    private float _perfectMultiplier = 1.5f;
    [SerializeField]
    private float _outOfBeatMultiplier = 0.5f;

    private float _perfectID = 0f;
    private float _normalID = 1f;
    private float _missedID = 2f;

    [SerializeField]
    private float _songBPM;
    static public float _currentBPM;
    [SerializeField]
    private float _secPerBeat;
    [SerializeField]
    private float _songPosition;
    private float _secondsDiff;
    [SerializeField]
    private float _songPositionInBeats;
    private float _beatDiff;
    static public float _dspSongTime;

    [Header("Music")]
    [SerializeField]
    private AudioClip _musicLoop;
    [SerializeField]
    private AudioClip _shopMusicIntro;
    [SerializeField]
    private AudioClip _shopMusicLoop;
    [SerializeField]
    private AudioClip _shopMusicOutro;
    [SerializeField]
    private AudioClip[] _levelMusicClips;

    public static bool _gamePaused = false;
    public static bool _isAtShop = false;

    [SerializeField]
    private float _fadeTime;
    private float _startingVolume;
    private int _currentLevel = 0;

    [Header("Player")]
    [SerializeField]
    private RowingMovement _playerController;

    [SerializeField]
    private Animator _svenAnimator;
    private bool _svenSwitch;

    [Header("UI")]
    [SerializeField]
    private GameObject _leftSpawner;
    [SerializeField]
    private GameObject _rightSpawner;
    [SerializeField]
    private GameObject _linePrefab;
    [SerializeField]
    private int _beatsShown = 2;
    static public int _beatNextIndex = 0;
    static public int _lineDirection;

    private float _pauseAudioDSP;
    private float _unpauseAudioDSP;

    private bool _levelStart = true;

    private float _drumCount;
    [SerializeField]
    private Animator _drumAnimator;

    [SerializeField]
    private TextMeshProUGUI _countdown;


    private Vector2 _vectorID = new Vector2();

    private float _timer;

    private void OnEnable()
    {
        _playerController.GamePaused += PauseMusic;
    }
    private void OnDisable()
    {
        _playerController.GamePaused -= PauseMusic;
    }
    
    private void Awake()
    {
        _audioSource = gameObject.GetComponent<AudioSource>();

        _timer = _beatLength;

        _secPerBeat = 60f / _songBPM;

        _startingVolume = _audioSource.volume;

        _currentBPM = _songBPM;

        _gamePaused = false;

        _levelStart = true;

        _beatNextIndex = 0;
    }

    private void Start()
    {
        _dspSongTime = (float)AudioSettings.dspTime;
        _audioSource.Play();

    }

    void Update()
    {
        // _timer -= Time.deltaTime;
        // if (_timer <= 0)
        // {
        //     // _clipIndex = UnityEngine.Random.Range(0,1);
        //     // _audioSource.PlayOneShot(_drumBeatSound[_clipIndex], _volume);

        //     _timer = _beatLength;
        // }
        
        if (_levelStart == true)
        {
            if (_songPositionInBeats > 0.9 && _songPositionInBeats < 1.1)
            {
                _countdown.text = "3";
            }
            if (_songPositionInBeats > 1.9 && _songPositionInBeats < 2.1)
            {
                _countdown.text = "2";
            }
            if (_songPositionInBeats > 2.9 && _songPositionInBeats < 3.1)
            {
                _countdown.text = "1";
            }
            if (_songPositionInBeats > 3.9 && _songPositionInBeats < 4.1)
            {
                _countdown.text = "Go!";
                RowingMovement.GiveControl();
            }
            if (_songPositionInBeats > 4.5 && _songPositionInBeats < 5)
            {
                _countdown.text = " ";
                _countdown.gameObject.SetActive(false);
                _levelStart = false;
            }
        }

        if (_gamePaused == false)
        {
            _songPosition = (float)(AudioSettings.dspTime - _dspSongTime);
            // _songPosition -= 0.2f;
            _songPositionInBeats = _songPosition / _secPerBeat;

            //Play UI Animation of Drum
            _drumCount = _songPositionInBeats - (Mathf.Floor(_songPositionInBeats));
            if (_drumCount > 0.95 || _drumCount < 0.05)
            {
                _drumAnimator.Play("Beat", 0);
            }
            //Play Sven's Drumming Animation
            if (_drumCount > 0.9)
            {
                if (_svenSwitch == true)
                {
                    _svenAnimator.Play("LeftDrum", 0);
                    _svenSwitch = false;
                }
                else
                {
                    _svenAnimator.Play("RightDrum", 0);
                    _svenSwitch = true;
                }
            }
            

            if (_beatNextIndex < _songPositionInBeats + _beatsShown)
            {
                _lineDirection = 1;
                Instantiate(_linePrefab, _leftSpawner.transform.position, _leftSpawner.transform.rotation, _leftSpawner.transform);

                _lineDirection = -1;
                Instantiate(_linePrefab, _rightSpawner.transform.position, _rightSpawner.transform.rotation, _rightSpawner.transform);


                _beatNextIndex ++;
            }            
        }

        // if (_isAtShop == true && _audioSource.isPlaying == false)
        // {
        //     _audioSource.clip = _shopMusicLoop;
        //     _audioSource.Play();
        // }
    }

    public Vector2 GetBeatTime()
    {
        float _inputBeatPosition = _songPositionInBeats;
        float _comparedBeat = Mathf.Round(_songPositionInBeats);

        float _difference = _inputBeatPosition - _comparedBeat;
        // Debug.Log(_difference);

        if (_difference <= _perfectBeat && _difference >= -_perfectBeat)
        {
            _vectorID.x = _perfectMultiplier;
            _vectorID.y = _perfectID;


            return _vectorID;
        }
        else if (_difference > _normalBeat || _difference < -_normalBeat)
        {
            _vectorID.x = _outOfBeatMultiplier;
            _vectorID.y = _missedID;
            return _vectorID;
        }
        else
        {
            _vectorID.x = 1f;
            _vectorID.y = _normalID;

            return _vectorID;
        }
    }

    public float BeatLenght()
    {
        return _beatLength;
    }

    public IEnumerator FadeOutAudio()
    {
        //Might take this out when adding new songs, since they would start from the begining
        _isAtShop = true;

        while (_audioSource.volume > 0 && _isAtShop == true)
        {
            _audioSource.volume -= _startingVolume * Time.fixedUnscaledDeltaTime / _fadeTime;


            yield return null;
        }
        _audioSource.Stop();

        _currentLevel ++;


        _audioSource.loop = true;
        _audioSource.clip = _shopMusicLoop;
        _audioSource.volume = _startingVolume;
        _audioSource.Play();

        // if (true)
        // {
            
        // }
    }
    public void RestartMusic()
    {
        _isAtShop = false;

        _audioSource.volume = _startingVolume;

        _audioSource.loop = true;
        _audioSource.clip = _levelMusicClips[_currentLevel];
        _audioSource.Play();

        _dspSongTime = (float)AudioSettings.dspTime;
        _beatNextIndex = 0;
    }

    public void PauseMusic()
    {
        if (_gamePaused == false)
        {
            _gamePaused = true;
            _audioSource.Pause();
            
            GetAudioDifference();
        }
        else
        {
            _audioSource.UnPause();
            _gamePaused = false;

            SetAudioDifference();
        }
    }

    private void GetAudioDifference()
    {
        float _roundedPosition = Mathf.Floor(_songPosition);
        float _roundedBeat = Mathf.Floor(_songPositionInBeats);

        _pauseAudioDSP = (float)AudioSettings.dspTime;

        _secondsDiff = _songPosition - _roundedPosition;
        _beatDiff = _songPositionInBeats - _roundedBeat;
    }
    private void SetAudioDifference()
    {
        _unpauseAudioDSP = (float)AudioSettings.dspTime;

        float _difference = _unpauseAudioDSP - _pauseAudioDSP;
        
        _dspSongTime += _difference;

        _songPosition = _secondsDiff;
        _songPositionInBeats = _beatDiff;
    }

    public void FadeOutShop()
    {
        StartCoroutine("FadeOutShopMusic");
    }
    private IEnumerator FadeOutShopMusic()
    {
        while (_audioSource.volume > 0)
        {
            _audioSource.volume -= _startingVolume * Time.fixedUnscaledDeltaTime / _fadeTime;

            Debug.Log("Music Volume is " + _audioSource.volume);

            yield return null;
        }
    }

    public void EnableCountdown()
    {
        _countdown.gameObject.SetActive(true);
        _levelStart = true;
    }
}
