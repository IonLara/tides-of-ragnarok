﻿using UnityEngine;

public class BoatSounds : MonoBehaviour
{
    private Rigidbody _rigidBody;

    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private AudioClip _idleSound;
    [SerializeField]
    private AudioClip[] _movingSound;
    [SerializeField]
    private float _idleVelocity = 2;
    [SerializeField]
    private float _moveVolume = 0.5f;
    [SerializeField]
    private float _idleVolume = 0.8f;

    private bool _isPlaying = false;
    private bool _isMoving = false;

    private void Awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        _isPlaying = _audioSource.isPlaying;
        _audioSource.volume = _idleVolume;
        if (_rigidBody.velocity.magnitude < _idleVelocity)
        {
            if (_isPlaying == false)
            {
                _audioSource.clip = _idleSound;
                _audioSource.Play();
                _isMoving = false;                
            }
        }
        else
        {
            _audioSource.volume = _moveVolume;
            if (_isMoving == false)
            {
                _audioSource.Stop();
                int clipIndex = UnityEngine.Random.Range(0, _movingSound.Length);
                _audioSource.clip = _movingSound[clipIndex];
                _audioSource.Play();         
                _isMoving = true;       
            }
            // _audioSource.Stop();
            if (_isPlaying == false)
            {
                _isMoving = false;
            }

        }
    }
}
