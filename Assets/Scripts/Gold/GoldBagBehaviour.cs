﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class GoldBagBehaviour : MonoBehaviour
{
    [SerializeField]
    private float _goldAmount = 100;

    [SerializeField]
    private GameObject _coinParticles;

    [SerializeField]
    private AudioSource _feedbackSource;
    [SerializeField]
    private AudioClip _coinSound;
    [SerializeField]
    private AudioClip[] _playerGoldSounds;

    [SerializeField]
    private int _playerLayer;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == _playerLayer)
        {
            other.gameObject.GetComponent<RowingMovement>().ChangeCurrentGold(_goldAmount);

            _feedbackSource.PlayOneShot(_coinSound);

            int _clipIndex = UnityEngine.Random.Range(0, _playerGoldSounds.Length);
            _feedbackSource.clip = _playerGoldSounds[_clipIndex];
            _feedbackSource.PlayDelayed(0.5f); 

            this.gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
        Instantiate(_coinParticles, transform.position, transform.rotation);
    }
}
