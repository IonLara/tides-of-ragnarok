﻿using UnityEngine;

public class BoatRotationConstraint : MonoBehaviour
{
    private Rigidbody _rigidBody;

    [SerializeField]
    private float _maxRotation = 10f;
    [SerializeField]
    private float _fixForce = 30f;

    private void Awake()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(gameObject.transform.localRotation.z >= 10 || gameObject.transform.localRotation.z <= -10)
        {
            Debug.Log("Balancing!");
            _rigidBody.AddTorque(0,0,_fixForce, ForceMode.Force);
        }
    }
}
