%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: LeftArmMask
  m_Mask: 00000000000000000000000000000000000000000100000000000000010000000000000000000000000000000100000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Front_Must_CTRL
    m_Weight: 1
  - m_Path: Grounded
    m_Weight: 1
  - m_Path: Grounded/Hip
    m_Weight: 1
  - m_Path: Grounded/Hip/L_KiltStart
    m_Weight: 1
  - m_Path: Grounded/Hip/L_KiltStart/L_KiltMiddle
    m_Weight: 1
  - m_Path: Grounded/Hip/L_KiltStart/L_KiltMiddle/L_KiltEnd
    m_Weight: 1
  - m_Path: Grounded/Hip/L_UpperLeg
    m_Weight: 1
  - m_Path: Grounded/Hip/L_UpperLeg/L_Leg
    m_Weight: 1
  - m_Path: Grounded/Hip/L_UpperLeg/L_Leg/L_Foot
    m_Weight: 1
  - m_Path: Grounded/Hip/L_UpperLeg/L_Leg/L_Foot/L_FootBend
    m_Weight: 1
  - m_Path: Grounded/Hip/L_UpperLeg/L_Leg/L_Foot/L_FootBend/L_Toe
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm/L_LowerArm
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm/L_LowerArm/L_Hand
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm/L_LowerArm/L_Hand/L_Fingers1
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm/L_LowerArm/L_Hand/L_Fingers1/L_Fingers2
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/L_Clav/L_UpperArm/L_LowerArm/L_Hand/L_Fingers1/L_Fingers2/L_Fingers3
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/Beard
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/Beard/Beard_End
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/L_MustacheBase
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/L_MustacheBase/L_MustacheBody
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/L_MustacheBase/L_MustacheBody/L_MustacheTip
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/R_MustacheBase
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/R_MustacheBase/R_MustacheBody
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/Neck/Head/Face/R_MustacheBase/R_MustacheBody/R_MustacheTip
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm/R_LowerArm
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm/R_LowerArm/R_Hand
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm/R_LowerArm/R_Hand/R_Fingers1
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm/R_LowerArm/R_Hand/R_Fingers1/R_Fingers2
    m_Weight: 1
  - m_Path: Grounded/Hip/LowerBack/Chest/Collarbone/R_Clav/R_UpperArm/R_LowerArm/R_Hand/R_Fingers1/R_Fingers2/R_Fingers3
    m_Weight: 1
  - m_Path: Grounded/Hip/M_KiltStart
    m_Weight: 1
  - m_Path: Grounded/Hip/M_KiltStart/M_KiltMid
    m_Weight: 1
  - m_Path: Grounded/Hip/M_KiltStart/M_KiltMid/M_KiltEnd
    m_Weight: 1
  - m_Path: Grounded/Hip/R_KiltStart
    m_Weight: 1
  - m_Path: Grounded/Hip/R_KiltStart/R_KiltMiddle
    m_Weight: 1
  - m_Path: Grounded/Hip/R_KiltStart/R_KiltMiddle/R_KiltEnd
    m_Weight: 1
  - m_Path: Grounded/Hip/R_UpperLeg
    m_Weight: 1
  - m_Path: Grounded/Hip/R_UpperLeg/R_Leg
    m_Weight: 1
  - m_Path: Grounded/Hip/R_UpperLeg/R_Leg/R_Foot
    m_Weight: 1
  - m_Path: Grounded/Hip/R_UpperLeg/R_Leg/R_Foot/R_FootBend
    m_Weight: 1
  - m_Path: Grounded/Hip/R_UpperLeg/R_Leg/R_Foot/R_FootBend/R_Toe
    m_Weight: 1
  - m_Path: Head_CTRL_offset
    m_Weight: 1
  - m_Path: Head_CTRL_offset/Head_CTRL
    m_Weight: 1
  - m_Path: ikHandle1
    m_Weight: 1
  - m_Path: ikHandle2
    m_Weight: 1
  - m_Path: ikHandle3
    m_Weight: 1
  - m_Path: ikHandle4
    m_Weight: 1
  - m_Path: ikHandle5
    m_Weight: 1
  - m_Path: ikHandle6
    m_Weight: 1
  - m_Path: ikHandle7
    m_Weight: 1
  - m_Path: ikHandle8
    m_Weight: 1
  - m_Path: L_Clav_CTRL_offset
    m_Weight: 1
  - m_Path: L_Clav_CTRL_offset/L_Clav_CRTL
    m_Weight: 1
  - m_Path: L_Elbow_CTRL
    m_Weight: 1
  - m_Path: L_Foot_CTRL
    m_Weight: 1
  - m_Path: L_Knee_CTRL
    m_Weight: 1
  - m_Path: L_Wrist_CTRL_offset
    m_Weight: 1
  - m_Path: L_Wrist_CTRL_offset/L_Wrist_CTRL
    m_Weight: 1
  - m_Path: Low_Back_CTRL
    m_Weight: 1
  - m_Path: R_Clav_CTRL_Offset
    m_Weight: 1
  - m_Path: R_Clav_CTRL_Offset/R_Clav_CTRL
    m_Weight: 1
  - m_Path: R_Elbow_CTRL
    m_Weight: 1
  - m_Path: R_Foot_CTRL
    m_Weight: 1
  - m_Path: R_Knee_CTRL
    m_Weight: 1
  - m_Path: R_Wrist_CTRL_offset
    m_Weight: 1
  - m_Path: R_Wrist_CTRL_offset/R_Wrist_CTRL
    m_Weight: 1
  - m_Path: Ragnar
    m_Weight: 1
  - m_Path: Root_CTRL
    m_Weight: 1
  - m_Path: Root_CTRL/Root_CTRL 1
    m_Weight: 1
  - m_Path: Up_Back_CTRL
    m_Weight: 1
