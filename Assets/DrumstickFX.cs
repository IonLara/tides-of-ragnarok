﻿using UnityEngine;

public class DrumstickFX : MonoBehaviour
{
    [SerializeField]
    private GameObject _particles;

    private void SpawnParticles()
    {
        Instantiate(_particles, transform);
    }
}
