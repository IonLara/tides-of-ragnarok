﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{

    float currentSize;
    Vector3 initialSize;
    [SerializeField]
    float targetSize;
    [SerializeField]
    float lerpDuration;
    // Start is called before the first frame update
    Coroutine growCoroutine;

    void Start()
    {
        initialSize = transform.localScale;
        currentSize = 1.0f;
        growCoroutine = StartCoroutine(GrowToSize());
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P)){
            StopCoroutine(growCoroutine);
        }
    }


    public IEnumerator GrowToSize(){
        yield return new WaitForSeconds(10.0f);
        
        float lerpTime = 0.0f;        

        while(lerpTime <= lerpDuration){
            lerpTime += Time.deltaTime;
            if(lerpTime > lerpDuration){
                lerpTime = lerpDuration;
            }

            float percent = lerpTime / lerpDuration;
            transform.localScale = Vector3.Lerp(initialSize, initialSize * targetSize, percent);

           yield return null;
        }
        yield return null;
    }
}
